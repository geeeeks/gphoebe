<?php

/**
 * Declare common (backend|frontend) global functions here
 * but try not to use any global functions unless you need.
 */


if (!function_exists('followers_number')) {
	function followers_number()
	{
		return get_option('gphoebe_settings')['followers_number'] || 20;
	}
}

if (!function_exists('dd')) {
    function dd()
    {
        foreach (func_get_args() as $value) {
            echo '<pre>';
            print_r($value);
            echo '</pre><br>';
        }
        die;
    }
}

if (! function_exists('gphoebe_mix')) {
	/**
	 * Get the path to a versioned Mix file.
	 *
	 * @param  string  $path
	 * @param  string  $manifestDirectory
	 *
	 * @throws \Exception
	 */
	function gphoebe_mix($path, $manifestDirectory = '')
	{
		static $manifests = [];

		if (substr($path, 0, 1) !== '/') {
			$path = "/".$path;
		}

		if ($manifestDirectory && substr($manifestDirectory, 0, 1) !== '/') {
			$manifestDirectory = "/".$manifestDirectory;
		}

		$publicPath = \Gphoebe\App::publicPath();

		if (file_exists($publicPath.'/hot')) {
			return (is_ssl() ? "https" : "http")."://localhost:8080".$path;
		}

		$manifestPath = $publicPath.$manifestDirectory.'mix-manifest.json';

		if (! isset($manifests[$manifestPath])) {
			if (! file_exists($manifestPath)) {
				throw new Exception('The Mix manifest does not exist.');
			}

			$manifests[$manifestPath] = json_decode(file_get_contents($manifestPath), true);
		}

		$manifest = $manifests[$manifestPath];

		if (! isset($manifest[$path])) {
			throw new Exception(
				"Unable to locate Mix file: ".$path.". Please check your ".
				'webpack.mix.js output paths and try again.'
			);
		}

		return \Gphoebe\App::publicUrl($manifestDirectory.$manifest[$path]);
	}
}
