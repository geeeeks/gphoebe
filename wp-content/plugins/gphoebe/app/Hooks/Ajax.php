<?php

/**
 * Add all ajax hooks
 */

$pluginSlug = $app->getSlug();

/**
 * Get Settings Details
 */
$app->addAdminAjaxAction(
	$pluginSlug . '_get_settings_details',
	'Gphoebe\App\Modules\AjaxReq\Backend\SettingsController@getSettingsDetails'
);

/**
 * Store Settings Details
 */
$app->addAdminAjaxAction(
	$pluginSlug . '_post_settings_details',
	'Gphoebe\App\Modules\AjaxReq\Backend\SettingsController@storeSettingsData'
);

$app->addAdminAjaxAction(
	$pluginSlug . '_get_category',
	'Gphoebe\App\Modules\AjaxReq\Backend\SettingsController@getSettingsCategory'
);

$app->addAdminAjaxAction(
	$pluginSlug . '_store_category',
	'Gphoebe\App\Modules\AjaxReq\Backend\SettingsController@storeCategory'
);

/**
 * Get User Info Details
 */

$app->addAdminAjaxAction(
	$pluginSlug . '_post_user_profile_info',
	'Gphoebe\App\Modules\AjaxReq\Backend\UserInfo@storeUserData'
);



// $app->addAdminAjaxAction(
// 	$pluginSlug . '_submit_user_info',
// 	'Gphoebe\App\Modules\AjaxReq\Backend\UserInfo@storeUserData'
// );


/**
 * Get User Info Details
 */

// $app->addAdminAjaxAction(
// 	$pluginSlug . '_get_user_profile_info',
// 	'Gphoebe\App\Modules\AjaxReq\Backend\UserInfo@getUserData'
// );

/**
 * get social user account
 */
$app->addAdminAjaxAction(
	$pluginSlug . '_get_social_account',
	'Gphoebe\App\Modules\AjaxReq\Backend\InfluenceController@getSocialAccount'
);

/**
 * Store Social Account
 */
$app->addAdminAjaxAction(
	$pluginSlug . '_store_social_account',
	'Gphoebe\App\Modules\AjaxReq\Backend\InfluenceController@storeSocialAccount'
);

/**
 * Get All Influencers for main page
 */
$app->addAdminAjaxAction(
	$pluginSlug . '_get_influencers',
	'Gphoebe\App\Modules\AjaxReq\Backend\MainPageController@getAccount'
);


$app->addPublicAjaxAction(
	$pluginSlug . '_get_influencers',
	'Gphoebe\App\Modules\AjaxReq\Backend\MainPageController@getAccount'
);


$app->addAdminAjaxAction(
	$pluginSlug . '_store_packages',
	'Gphoebe\App\Modules\AjaxReq\Backend\InfluenceController@storeSocialAccount'
);

$app->addAdminAjaxAction(
	$pluginSlug . '_store_transaction_details',
	'Gphoebe\App\Modules\AjaxReq\Backend\Transaction@storeTransactionWithPaymentAndBalance'
);


$app->addAdminAjaxAction(
	$pluginSlug . '_get_users',
	'Gphoebe\App\Modules\AjaxReq\Backend\ReferLinkModifier@getUsers'
);

$app->addAdminAjaxAction(
	$pluginSlug . '_store_refer_users',
	'Gphoebe\App\Modules\AjaxReq\Backend\ReferLinkModifier@storeUsers'
);

$app->addAdminAjaxAction(
	$pluginSlug . '_get_withdrawl_details',
	'Gphoebe\App\Modules\AjaxReq\Backend\Withdrawl@getAllWithdrawl'

);