<?php

/**
 * Declare backend actions/filters/shortcodes
 */

$pluginSlug = $app->getSlug();

/**
 * Register Post Type
 */
$app->addAction(
	'init',
	function() {
		new Gphoebe\App\Modules\PostType\RegisterPostType();
	}
);

// $app->addAction(
// 	'init',
// 	'Gphoebe\App\Modules\AjaxReq\Backend\UserInfo@getUserData'
// );

/**
 * Register Admin Menu
 */
$app->addAction(
	'admin_menu',
	'Gphoebe\App\Modules\AdminMenu\RegisterAdminMenu@registerMenu'
);