<?php

/**
 * Declare common actions/filters/shortcodes
 */

$app->addAction(
	'wp_logout',
	function (){
		wp_redirect( home_url() );
		exit();
	}
);

//$app->addAction(
//	'admin_init',
//	function() {
//		$redirect = isset( $_SERVER['HTTP_REFERER'] ) ? $_SERVER['HTTP_REFERER'] : home_url( '/' );
//		if (!current_user_can('administrator') && !is_admin()) {
//			exit( wp_redirect( $redirect ) );
//		}
//	}
//);