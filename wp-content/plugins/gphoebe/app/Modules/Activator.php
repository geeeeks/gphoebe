<?php

namespace Gphoebe\App\Modules;

class Activator
{
    /**
     * This method will be called on plugin activation
     */
    public function handleActivation()
    {
    	$this->createGphoebeUsers();
    	$this->createGphoebeUserSocialAccount();
    	$this->createGphoebePricing();
    	$this->createGphoebeUserCategory();
    	$this->createGphoebeFavorites();
    	$this->createGphoebeCategory();
    	$this->createGphoebeTransaction();
    	$this->createGphoebeTransformToken();
    	$this->createGphoebePayment();
    	$this->createGphoebeOrderTable();
    	$this->createGphoebeOrderDetails();
    	$this->createGphoebeCurrentBalance();
    	$this->createGphoebeInvitationUsers();
    	$this->createGphoebeWithdrawRequest();
        $this->createSendShortCode();
    }

    public function createSendShortCode() {
        global $wpdb;

        $tableName = $wpdb->prefix . 'gphoebe_short_code';

        if (! self::tableExists($tableName)) {
            $sql = "CREATE TABLE $tableName (
                id int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
                user_id int(11) NOT NULL,
                order_id int(11) NOT NULL,
                sc VARCHAR(50) NOT NULL,
                gshortcode VARCHAR(50) NULL
             )";

             self::runSQL($sql);
        }
    }

    public function createGphoebeWithdrawRequest()
    {
    	global $wpdb;

    	$tableName = $wpdb->prefix . 'gphoebe_withdraw_request';

    	if (! self::tableExists($tableName)) {
    		$sql = "CREATE TABLE $tableName (
				id int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
				request_by int(11) NOT NULL,
				request_method ENUM('paypal', 'card') NULL,
				request_amount DECIMAL(10, 2) NOT NULL,
				request_email VARCHAR(100) NULL,
				request_card_number VARCHAR (16),
				request_card_expire VARCHAR(10),
				request_cvc int(3),
				request_status ENUM('paid', 'pending') DEFAULT 'pending',
				created_at TIMESTAMP ,
				updated_at TIMESTAMP
			)";
    		self::runSQL($sql);
	    }
    }

    public function createGphoebeInvitationUsers()
    {
    	global $wpdb;

    	$tableName = $wpdb->prefix . 'gphoebe_invitation';

    	if (! self::tableExists($tableName)) {
    		$sql = "CREATE TABLE $tableName (
				id int(11) AUTO_INCREMENT PRIMARY KEY,
				buyer_id int(11) NOT NULL,
				influencer_id int(11) NOT NULL,
				order_id int(11) NOT NULL,
				order_details_id int(11) NOT NULL,
				status ENUM('accepted', 'pending' , 'reject') DEFAULT 'pending',
				created_at TIMESTAMP ,
				updated_at TIMESTAMP 
			)";

    		self::runSQL($sql);
	    }
    }

    public function createGphoebeCurrentBalance()
    {
    	global $wpdb;

    	$tableName = $wpdb->prefix . 'gphoebe_user_balance';

    	if (! self::tableExists($tableName)) {
    		$sql = "CREATE TABLE $tableName (
				id int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
				user_id int(11) NOT NULL,
				current_balance int(11) NOT NULL DEFAULT 0
			)";

    		self::runSQL($sql);
	    }
    }

    public function createGphoebeOrderDetails()
    {
    	global $wpdb;

    	$tableName = $wpdb->prefix . 'gphoebe_order_details';

    	if (! self::tableExists($taleName)) {
    		$sql = "CREATE TABLE $tableName (
				id int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
				order_id int(11) NOT NULL,
				order_description VARCHAR(255) NOT NULL,
				order_start_at TIMESTAMP ,
				order_end_at TIMESTAMP,
				order_amount DECIMAL(10, 2) NOT NULL,
				order_status ENUM('completed', 'delivered', 'pending', 'revision' ,'cancelled', 'invited') DEFAULT 'invited',
				payment_status ENUM('paid', 'pending') DEFAULT 'pending', 
				influencer_id int(11) NOT NULL, 
				buyer_id int(11) NOT NULL,
				created_at TIMESTAMP,
				updated_at TIMESTAMP
			)";

    		self::runSQL($sql);
	    }
    }

	/**
	 * Order Table
	 */
    public function createGphoebeOrderTable()
    {
    	global  $wpdb;

    	$tableName = $wpdb->prefix . 'gphoebe_orders';

    	if (! self::tableExists($tableName)) {
    		$sql = "CREATE TABLE $tableName (
				id int(11) AUTO_INCREMENT PRIMARY KEY NOT NULL,
				order_id VARCHAR (100) NOT NULL, 
				buyer_id int(11) NOT NULL, 
				total_amount DECIMAL (10, 2) NOT NULL,
				total_order int(11) NOT NULL,
				order_status ENUM('completed', 'delivered', 'pending', 'revision' ,'cancelled', 'invited') DEFAULT 'invited',
				payment_status ENUM('paid', 'pending') DEFAULT 'pending',
				created_at TIMESTAMP ,
				updated_at TIMESTAMP 
			)";
    		self::runSQL($sql);
	    }
    }

	/**
	 * Payment Table when user/ buyer withdraw
	 */
    public function createGphoebePayment()
    {
    	global $wpdb;

    	$tableName = $wpdb->prefix . 'gphoebe_payment';

    	if (! self::tableExists($tableName)) {
    		$sql = "CREATE TABLE $tableName (
				transaction_id int(11) NOT NULL,
				user_to int(11) NOT NULL, 
				user_by int(11) NOT NULL, 
				real_amount DECIMAL (10, 2) NOT NULL,
				convert_token DECIMAL (10, 2) NOT NULL,
				created_at TIMESTAMP ,
				updated_at TIMESTAMP 
			)";

    		self::runSQL($sql);
	    }
    }

	/**
	 * Convert Add Fund Token Table
	 */
    public function createGphoebeTransformToken()
    {
    	global $wpdb;

    	$tableName = $wpdb->prefix . 'gphoebe_transform_token';

    	if (! self::tableExists($tableName)) {
    		$sql = "CREATE TABLE $tableName (
				transaction_id int(11) NOT NULL,
				user_by int(11) NOT NULL, 
				type ENUM('withdraw', 'add') DEFAULT 'add',
				real_amount DECIMAL (10, 2) NOT NULL,
				convert_token DECIMAL (10, 2) NOT NULL,
				created_at TIMESTAMP ,
				updated_at TIMESTAMP 
			)";

    		self::runSQL($sql);
	    }
    }

	/**
	 *  Transaction Table
	 */
    public function createGphoebeTransaction()
    {
    	global $wpdb;

    	$tableName = $wpdb->prefix . 'gphoebe_transaction';

    	if (! self::tableExists($tableName)) {
    		$sql = "CREATE TABLE $tableName (
				id int(11) AUTO_INCREMENT PRIMARY KEY NOT NULL,
				transaction_id VARCHAR(200) NOT NULL,
				transaction_type ENUM('paypal', 'stripe') NOT NULL,
				amount DECIMAL(10,2) NOT NULL,
				type ENUM('withdraw', 'add') NOT NULL,
				user_by int(11) NOT NULL,
				note VARCHAR(255) NULL,
				created_at TIMESTAMP ,
				updated_at TIMESTAMP 
			)";

    		self::runSQL($sql);
	    }
    }


    public function createGphoebeUsers()
    {
    	global $wpdb;

    	$tableName=$wpdb->prefix.'gphoebe_users';
    	if(! self:: tableExists($tableName))
    		{
    			$sql="CREATE TABLE $tableName(
    				user_id int(11) NOT NULL,
    				user_name VARCHAR(100) NULL,
    				phone_number VARCHAR(20),
    				registration_date DATETIME,
    				language VARCHAR(100),
    				country VARCHAR(50),
    				age VARCHAR (20),
    				sex ENUM ('male','female'),
    				activity VARCHAR(100),
    				is_buyer ENUM('0', '1') DEFAULT 1,
    				description TEXT NULL,
                    refer_id VARCHAR(100) NULL
    		)";
    		self::runSQL($sql);
    		}
    }

    public function createGphoebeUserSocialAccount()
    {
    	global $wpdb;

    	$tableName = $wpdb->prefix.'gphoebe_user_social_account';
    	if(! self:: tableExists($tableName))
    		{
    			$sql="CREATE TABLE $tableName(
    				user_id int(11) NOT NULL,
    				account_type VARCHAR(50) NULL,
    				user_name VARCHAR(100)NULL,
    				social_user_id VARCHAR(50) NULL,
    				followers int(11) NULL,
    				dp VARCHAR(512) NULL
    		)";
    		self::runSQL($sql);
    		}
    }
    public function createGphoebePricing()
    {
    	global $wpdb;

    	$tableName = $wpdb->prefix . 'gphoebe_pricing';
    	if(! self:: tableExists($tableName))
    		{
    			$sql= "CREATE TABLE $tableName(
                    id int(11) PRIMARY KEY AUTO_INCREMENT,
    				user_id int(11) NOT NULL,
    				duration DECIMAL(10, 2) NOT NULL,
    				description VARCHAR(191) NOT NULL,
    				price DECIMAL(10,2) NOT NULL,
    				display int(1) NOT NULL

    		)";
    		self::runSQL($sql);

    		}
    }

    public function createGphoebeCategory()
    {
    	global $wpdb;

    	$tableName = $wpdb->prefix . 'gphoebe_category';

    	if(! self::tableExists($tableName) ) {
    		$sql = "CREATE TABLE $tableName (
    			id int(11) PRIMARY KEY AUTO_INCREMENT NOT NULL,
    			category_name VARCHAR(100) NOT NULL
    		)";

    		self::runSQL($sql);
	    }
    }

    public function createGphoebeUserCategory()
    {
    	global $wpdb;

    	$tableName = $wpdb->prefix . 'gphoebe_user_category';

    	if(! self:: tableExists($tableName)){
    		$sql = "CREATE TABLE $tableName(
    				user_id int(11) NOT NULL,
    				category_id int(11) NOT NULL
    	)";

    	self::runSQL($sql);
    	}
    }

    public function createGphoebeFavorites()
    {
    	global $wpdb;

    	$tableName = $wpdb->prefix . 'gphoebe_favorities';

    	if(! self::tableExists($tableName))
    		{
    			$sql = "CREATE TABLE $tableName(
    					user_id int(11) NOT NULL,
    					fav_id int(11) NOT NULL
    		)";

    		self::runSQL($sql);
    		}
    }

    public function createGphoebeOrders()
    {
        global $wpdb;

        $tableName = $wpdb->prefix . 'gphoebe_orders';

        if(!self::tableExists($tableExists))
            {
                $sql = "CREATE TABLE $tableName(
                        user_id int(11) NOT NULL,
                        order_id int(11) NOT NULL AUTO_INCREMENT,
                        type VARCHAR(50) NOT NULL,
                        orders ENUM ('0','1')
                )";

            self::runSQL($sql);
            }
    }

	/**
	 * check table exists or not
	 * @param  $table_name
	 * @return query
	 */
	private static function tableExists($table_name){
		global $wpdb;
		return  $wpdb->get_var( "SHOW TABLES LIKE '$table_name'" ) == $table_name;
	}

	/**
	 * Run Sql Here
	 * @param  $sql
	 */
	private static function runSQL($sql) {
		global $wpdb;
		$charset_collate = $wpdb->get_charset_collate();
		require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
		dbDelta( $sql. " $charset_collate;" );
	}
}
