<?php

namespace Gphoebe\App\Modules;

use Gphoebe\App;

class AjaxHandler {


	public $request;
	public $app;
	public $gphoebe_users;
	public $user;
	public $gphoebe_pricing;
	public $gphoebe_user_category;
	
	public function __construct()
	{
		$this->ghoebe_user_category = 'gphoebe_user_category';
		$this->gphoebe_pricing = 'gphoebe_pricing';
		$this->user = 'user';
		$this->gphoebe_users ='gphoebe_users';
		$this->app = App::make();
		$this->request = App::make('request');
	}

	protected function responseSuccess( $data ) {
		wp_send_json_success( $data, 200 );
		die();
	}

	protected function responseError( $message = 'Something is wrong! Please try again' ) {
		wp_send_json_error( $message, 422 );
		die();
	}


}