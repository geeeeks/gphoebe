<?php

namespace Gphoebe\App\Modules\AjaxReq\Backend;

use Gphoebe\App\Modules\AjaxHandler;

class BrowserController extends AjaxHandler{

	public function __construct(){
		parent::__construct();
	}

	public function showSortByFollowers(){
		
		$user_details = wpFluent()->table('gphoebe_user_social_account')->orderBy('followers','DESC')->get();

		foreach($user_details as $key => $value){
			$value['price'] = wpFluent()->table('gphoebe_user_pricing')
										->where('user_id', $value['user_id'])
										->where('display',1)
										->first();
		}

		$this->responseSuccess(array(
			'status' => 'success',
			'user_details' =>$user_details
		));
	}

	public function showSortByPrice(){

		$user_details = wpFluent()->table('gphoebe_pricing')->orderBy('price','DESC')->get();

		foreach ($user_details as $key => $value) {
			$value['user_social_account'] = wpFuent()->table('gphoebe_user_social_account')
													 ->where('user_id',$value['user_id'])
													 ->where('display',1)
													 ->first();	

			$value['followers'] = wpFluent()->table('gphoebe_user_social_account')
											->where('user_id',$value['user_id'])
											->first();
		}

		$this->responseSuccess(array(
			'status' => 'success',
			'user_details' => $user_details

		));
	}
}