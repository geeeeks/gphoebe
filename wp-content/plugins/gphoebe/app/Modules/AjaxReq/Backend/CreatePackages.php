<?php

namespace Gphoebe\App\Modules\AjaxReq\Backend;

use Gphoebe\App\Modules\AjaxHandler;


class CreatePackage extends AjaxHandler{

	public function __construct()
	{
		parent::__construct();
	}

	public function createPackage(){
		$user_id = get_current_user_id();

		$description = sanitize_text_field($this->request->get('description'));

		if(! $description)
		{
			$this->responseError(array(
				'status' => 'warning',
				'message' => 'Please Provide Description!'
			));
		}
		
		$price = sanitize_text_field($this->request->get('cost'));

		if(! $price)
		{
			$this->responseError(array(
				'status' => 'warning',
				'message' => 'Please Provide Price!'
			));

		}

		$display = intval($this->request->get('display'));

		if(! $display)
		{
			$this->responseError(array(
				'status' => 'warning',
				'message' => 'Please Provide Display Value!'
			));

		}

		$check_user_id = wpFluent()->table($this->gphoebe_pricing)->where('user_id', $user_id)->first();


		$args = array(
				'user_id' 		=> $user_id,
				'description' 	=> $description,
				'price' 		=> $price,
				'display' 		=> $display
		);

		$display_arg_one = array(

			'display' => 1
		);

		$display_arg_zero = array(

			'display' => 0
		);


		if($display == 1)
		{
			wpFluent()->table($this->gphoebe_pricing)->where('user_id',$user_id)->update($display_arg_zero);

			wpFluent()->table($this->gphoebe_pricing)->insert($args);

			$this->responseSuccess(array(
				'status' => 'success',
				'message' => 'Successfully Created Package!'
			));
		}
		else
		{
			if(! $check_user_id)
			{

				wpFluent()->table($this->gphoebe_pricing)->insert($args);

				wpFluent()->table($this->gphoebe_pricing)->where('user_id',$user_id)->update($display_arg_one);

				$this->responseSuccess(array(
					'status' => 'success',
					'message' => 'Successfully Created Package!'
				));
			}
		}

	}
	public function updatePackage()
	{
		$user_id = get_current_user_id();

		$description = sanitize_text_field($this->request->get('description'));

		$time_duration = sanitize_text_field($this->request->get('time_duration'));

		$price = sanitize_text_field($this->request->get('price'));

		$package_id = intval(($this->request->get('package_id'));

		$display = intval($this->request->get('display'));

		$args = array(
				'user_id'		=> $user_id,
				'description'	=> $description,
				'price' 		=> $price,
				'display' 		=> $display
		);

		$display_arg_zero = array(

			'display' => 0
		);

		if($display == 1)
		{
			wpFluent()->table($this->gphoebe_pricing)->where('user_id',$user_id)->update($display_arg_zero);

			wpFluent()->table($this->gphoebe_pricing)->where('package_id', $package_id)->update($args);

			$this->responseSuccess(array(
				'status'  => 'success',
				'message' => 'Successfully Updated!'
			));
		}

		else
		{
			wpFluent()->table($this->gphoebe_pricing)->where('package_id', $package_id)->update($args);

			$this->responseSuccess(array(
				'status' => 'success',
				'message' => 'Successfully Updated!'
			));
		}

	}

	public function removePackage()
	{
		$package_id = intval(($this->request->get('package_id'));

		wpFluent()->table('gphoebe_pricing')->where('package_id')->delete();

		$this->responseSuccess(array(
			'status' => 'success',
			'message' => 'Successfully Removed Package!'
		));
	}
}