<?php
/**
 * Created by PhpStorm.
 * User: rafsan
 * Date: 3/26/18
 * Time: 5:34 PM
 */

namespace Gphoebe\App\Modules\AjaxReq\Backend;

use Gphoebe\App\Modules\AjaxHandler;

class GphoebeController extends  AjaxHandler {

	public function __construct() {
		parent::__construct();
	}

	/**
	 * Get Social Account
	 */
	public function getSocialAccount()
	{
		$userID = get_current_user_id();

		$getDetails = wpFluent()->table('gphoebe_user_social_account')->get();

		$this->responseSuccess(array(
			'message' => 'success',
			'user'    => $getDetails,
			'user_id' => $userID
		));
	}

}