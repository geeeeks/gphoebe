<?php
/**
 * Created by PhpStorm.
 * User: rafsan
 * Date: 3/26/18
 * Time: 5:34 PM
 */

namespace Gphoebe\App\Modules\AjaxReq\Backend;

use Gphoebe\App\Modules\AjaxHandler;

class InfluenceController extends  AjaxHandler {

	public function __construct() {
		parent::__construct();
	}

	/**
	 * Get Social Account
	 */
	public function getSocialAccount()
	{
		$userID = get_current_user_id();

		$getDetails = wpFluent()->table('gphoebe_user_social_account')->where('user_id', $userID)->get();

		$this->responseSuccess(array(
			'message' => 'success',
			'user'    => $getDetails,
			'user_id' => $userID
		));
	}

	/**
	 * store social account
	 * @throws \WpFluent\Exception
	 */
	public function storeSocialAccount()
	{
		$userId = get_current_user_id();

		$userName = sanitize_text_field( $this->request->get('user_name') );

		if (! $userName ) {
			$this->responseError(array(
				'status' => 'warning',
				'message' => 'Username required!'
			));
		}

		$followers = intval($this->request->get('followers'));

		if ($followers < 20 ) {
			$this->responseError(array(
				'status' => 'warning',
				'message' => 'Social Account Not Verfied!'
			));
		}



		$socialUserId = sanitize_text_field( $this->request->get('user_id') );

		$dpUrl = esc_url( $this->request->get('dp') );

		$args = array(
			'user_id' => $userId,
			'account_type' => 'instagram',
			'user_name' => $userName,
			'social_user_id' => $socialUserId,
			'followers' => $followers,
			'dp'        => $dpUrl
		);

		wpFluent()->table('gphoebe_user_social_account')->insert($args);

		$updateUser = array(
			'is_buyer' => '0'
		);

		wpFluent()->table('gphoebe_users')->where('user_id', $userId)->update($updateUser);

		$this->responseSuccess(array(
			'status' => 'success',
			'message' => 'Successfully Verified!'
		));

	}

	/**
	 * remove soical account
	 * @throws \WpFluent\Exception
	 */
	public function removeSocialAccount()
	{
		$userId = get_current_user_id();

		wpFluent()->table('gphoebe_user_social_account')->where('user_id')->delete();

		$this->responseSuccess(array(
			'status' => 'success',
			'message' => 'Successfully removed social account!'
		));
	}

}