<?php
/**
 * Created by PhpStorm.
 * User: rafsan
 * Date: 3/28/18
 * Time: 2:24 AM
 */

namespace Gphoebe\App\Modules\AjaxReq\Backend;

use Gphoebe\App\Modules\AjaxHandler;

class MainPageController extends  AjaxHandler {
	public function __construct()
	{
		parent::__construct();
	}
	public function getAccount()
	{
		$userID = get_current_user_id();

		$getDetails = wpFluent()->table('gphoebe_user_social_account')->orderBy('followers', 'DESC')->get();

		$modifiedGetDetails = array();

		foreach ($getDetails as $key => $value) {
			$getPriceDetails = wpFluent()->table('gphoebe_pricing')
			                             ->where('user_id', $value->user_id)
				                         ->where('display', 1)
										 ->first();
			if ($getPriceDetails) {
				$value->price = $getPriceDetails->price;

				array_push($modifiedGetDetails, $value);
			}
		}

		$this->responseSuccess(array(
			'message' => 'success',
			'user'    => $this->getTop18User($modifiedGetDetails)
		));
	}

	public function getTop18User(Array $arr)
	{
		return array_splice($arr, 0,18);
	}
}