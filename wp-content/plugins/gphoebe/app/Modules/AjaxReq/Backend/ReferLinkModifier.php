<?php

namespace Gphoebe\App\Modules\AjaxReq\Backend;

use Gphoebe\App\Modules\AjaxHandler;

class ReferLinkModifier extends AjaxHandler{

	public function __construct(){
		parent::__construct();
	}

	public function getUsers($message = '')
	{

		$users = wpFluent()->table('gphoebe_users')->orderBy('user_id', 'DESC')->get();

		$this->responseSuccess(array(
			'status' => 'success',
			'message' => $message,
			'users' => $users
		));
	}

	public function storeUsers()
	{
		$getUserId = intval( $this->request->get('user_id') );

		if (! $getUserId ) {
			$this->responseError(array(
				'status' => 'warning',
				'message' => 'User id missing!'
			));
		}

		$getUserName = sanitize_text_field( $this->request->get('user_name') );

		$getReferId = sanitize_text_field( $this->request->get('refer_id') );

		if (! $getReferId) {
			$this->responseError(array(
				'status' => 'warning',
				'message' => 'Refer id missing!'  
			));
		}

		$argsReferId = array(
			'refer_id' => $getReferId
		);

		wpFluent()->table('gphoebe_users')->where('user_id', $getUserId)->update($argsReferId);

		$this->getUsers('Successfully Updated!');
	}
}