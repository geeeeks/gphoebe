<?php
/**
 * Created by PhpStorm.
 * User: rafsan
 * Date: 4/1/18
 * Time: 10:38 PM
 */

namespace Gphoebe\App\Modules\AjaxReq\Backend;

use Gphoebe\App\Modules\AjaxHandler;

class Transaction extends AjaxHandler{

	public function __construct() {
		parent::__construct();
	}

	public function storeTransactionWithPaymentAndBalance()
	{
		$txnId = sanitize_text_field( $this->request->get('pay_id') );

		if (! $txnId) {
			$this->responseError(array(
				'status' => 'warning',
				'message' => 'Payment Id is missing!'
			));
		}

		$paymentType = sanitize_text_field( $this->request->get('payment_type') );

		if (! $paymentType) {
			$this->responseError(array(
				'status' => 'warning',
				'message' => 'Payment Method is required!'
			));
		}

		$amount = floatval( $this->request->get('amount') );

		if (! $amount) {
			$this->responseError(array(
				'status' => 'warning',
				'message' => 'Amount is required!'
			));
		}

		$userBy = intval( $this->request->get('buyer_id') );

		if (! $userBy) {
			$this->responseError(array(
				'status' => 'warning',
				'message' => 'Buyer Id required!'
			));
		}

		$type = sanitize_text_field( $this->request->get('type') );

		if (! $type) {
			$this->responseError(array(
				'status' => 'warning',
				'message' => 'Fund Type is required!'
			));
		}

		$note = sanitize_textarea_field( $this->request->get('note')  );

		$argsTransaction = array(
			'transaction_id' => $txnId,
			'transaction_type' => $paymentType,
			'amount'           => $amount,
			'type'             => $type,
			'user_by'          => $userBy,
			'note'             => $note,
			'created_at'	   => date( 'Y-m-d H:i:s'),
			'updated_at'  	   => date( 'Y-m-d H:i:s')
		);

		$insertId = wpFluent()->table('gphoebe_transaction')->insert($argsTransaction);

		$userTo = intval( $this->request->get('user_to') );

		if ( $type == 'withdraw' && $userTo) {
			$argsPayment = array(
				'transaction_id' => $insertId,
				'user_to'       => $userTo,
				'user_by'       => $userBy,
				'real_amount'   => $amount,
				'convert_token' => $amount,
				'created_at'	   => date( 'Y-m-d H:i:s'),
				'updated_at'  	   => date( 'Y-m-d H:i:s')
			);

			wpFluent()->table('gphoebe_payment')->insert($argsPayment);

			$getLastBalanceBuyer = wpFluent()->table('gphoebe_user_balance')
										->where('user_id', $userBy)
										->first()->current_balance;
			$argsBalanceBuyer = array(
				'current_balance' => $getLastBalanceBuyer - $amount < 0 ? : 0
			);

			wpFluent()->table('gphoebe_user_balance')->where('user_id', $userBy)
			                                         ->update($argsBalanceBuyer);

			if ($userTo != $userBy) {
				$getLastBalanceInf = wpFluent()->table('gphoebe_user_balance')
				                               ->where('user_id', $userTo)
				                               ->first()->current_balance;

				$argsBalanceInf = array(
					'current_balance' => $getLastBalanceInf + $amount
				);

				wpFluent()->table('gphoebe_user_balance')->where('user_id', $userTo)
				          ->update($argsBalanceInf);
			}

			$this->responseSuccess(array(
				'status' => 'success',
				'message' => 'Successfully Done!'
			));
		} else {
			$argsTransFormToken = array(
				'transaction_id' => $insertId,
				'user_by'       => $userBy,
				'type'          => $type,
				'real_amount'   => $amount,
				'convert_token' => $amount,
				'created_at'	   => date( 'Y-m-d H:i:s'),
				'updated_at'  	   => date( 'Y-m-d H:i:s')
			);

			wpFluent()->table('gphoebe_transform_token')->insert($argsTransFormToken);

			$getLastBalanceBuyer = wpFluent()->table('gphoebe_user_balance')
			                                 ->where('user_id', $userBy)
			                                 ->first()->current_balance;
			$argsBalanceBuyer = array(
				'current_balance' => $getLastBalanceBuyer + $amount
			);

			wpFluent()->table('gphoebe_user_balance')->where('user_id', $userBy)
			          ->update($argsBalanceBuyer);

			$this->responseSuccess(array(
				'status' => 'success',
				'message' => 'Successfully Done!'
			));
		}


	}

}