<?php

namespace Gphoebe\App\Modules\AjaxReq\Backend;

use Gphoebe\App\Modules\AjaxHandler;
use Gphoebe\App;

class UserInfo extends AjaxHandler{

	public function __construct()
	{
		parent::__construct();
	}
	public function storeUserData(){
		$user_id = get_current_user_id();
		
		$user_name = $this->request->get('user_name');
		
		$phone_number = sanitize_text_field( $this->request->get('phone_number') );
		
		$register_date = sanitize_text_field($this->request->get('register_date') );
		
		$languages = sanitize_text_field($this->request->get('languages') );
		
		$countries = sanitize_text_field($this->request->get('countries') );
		
		$age = intval($this->request->get('age') );
		
		$sex = sanitize_text_field($this->request->get('sex') );
		
		$activity = sanitize_text_field($this->request->get('activity') );

		$category = sanitize_text_field( $this->request-> get('category') );
		
		$description = sanitize_textarea_field($this->request->get('description') );

		$arg = array(
				'user_id' => $user_id,
				'user_name'=> $user_name,
				'phone_number' => $phone_number,
				'registration_date' => $register_date,
				'language' => $languages,
				'country' => $countries,
				'age' => $age,
				'sex' => $sex,
				'activity' => $activity,
				'description' => $description
		);

		//$user_id = get_current_user_id();

		$get_data= wpFluent()->table($this->gphoebe_users)->where('user_id', $user_id )->first();

		$get_categorie_data= wpFluent()->table($this->ghoebe_user_category)->where('user_id', $user_id )->first();

		$arg2 = array(
				'phone_number' => $phone_number,
				'language' => $languages,
				'country' => $countries,
				'age' => $age,
				'sex' => $sex,
				'activity' => $activity,
				'description' => $description
		);

		//return $get_data;

		// die();

		$category_data = array(
						'user_id' => $user_id,
						'category_id' => $category

		);

		if($get_categorie_data){
			wpFluent()->table($this->gphoebe_user_category)->where('user_id',$user_id)->update($category_data);

			$this->responseSuccess(array(
				'status' => 'success',
				'category_data' => $category_data

			));
		}

		else{

			wpFluent()->table($this->gphoebe_user_category)->insert($category_data);

			$this->responseSuccess(array(
				'status' => 'success',
				'category_data' => $category_data

			));
		}

		if($get_data){
			$user_id = get_current_user_id();

			wpFluent()->table($this->gphoebe_users)->where('user_id', $user_id )->update($arg2);
			$this->responseSuccess(array(
				'update' => true,
				'phone_number' => $phone_number,
				'language' => $languages,
				'country' => $countries,
				'age' => $age,
				'sex' => $sex,
				'activity' => $activity,
				'description' => $description
			));
		}
		else{

			wpFluent()->table($this->gphoebe_users)->insert($arg);

			$this->responseSuccess( array(
				'phone_number' => $phone_number,
				'language' => $languages,
				'country' => $countries,
				'age' => $age,
				'sex' => $sex,
				'activity' => $activity,
				'description' => $description
			) );
		}
	}

	public function getUserData()
	{

		$this->responseSuccess(array(
			'message' => '',
			'status'  => 'success',
			'user_info' => wpFluent()->table($this->gphoebe_users)
									 ->where( 'user_id', get_current_user_id() )
									 ->first()
		));

	}

	// public function changePassword(){
	// 	$user_name = $this->request->get('user_id');
	// 	$old_password = $this->request->get('old_password');
	// 	$new_password = $this->request->get('new_password');
	// 	$confirm_password = $this->request->get('confirm_password');
	// 	$check_old_password = wpFluent()->table($this->user)->where('user_login', $user_name)->get('user_password');
	// 	if(strcmp($old_password,$check_old_password)){
	// 			if(preg_match("/.{6,}/", $new_password)){
	// 				if(strcmp($new_password,$confirm_password)){
	// 					wpFluent()->table($this->user)->where('user_login', $user_name)->update('user_pass'=>$confirm_password);
	// 				}
	// 				else{
	// 					$errors['password_confirmation'] = "New Passwords do not match";	
	// 				}
	// 			}
	// 			else{
	// 				$errors['password'] = "Password must be at least six characters";  
	// 			}
	// 	}
	// 	else{
	// 		$errors['password_confirmation'] = "Old Passwords do not match";
	// 	}
	// }
}

?>