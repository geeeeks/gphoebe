<?php

namespace Gphoebe\App\Modules\AjaxReq\Backend;

use Gphoebe\App\Modules\AjaxHandler;
use Gphoebe\App;

class UserSocialAccount extends AjaxHandler{

	public function __construct(){
		parent::__construct();
	}
	public function getSocialAccount()
	{
		$user_id = get_current_user_id();

		$account_type = sanitize_text_field($this->request->get('account_type') );

		$user_name = sanitize_text_field($this->request->get('user_name') );

		$social_user_id = sanitize_text_field( $this->request->get('social_user_id') );

		$followers = sanitize_text_field($this->request->get('followers') );

		$check_account_exist = wpFluent()->table($this->gphoebe_user_social_account)
													->where('user_id',$user_id)
													->where('account_type',$account_type)
													->first();

		// $check_account_type = wpFluent()->table($this->gphoebe_user_social_account)->where('account_type',$)->first();

		if($check_account_exist){

				$this->responseSuccess(array(
				'status' => 'Error'
			));
		}

		else{
				$args = array(
					'user_id' => $user_id,
					'account_type' => $account_type,
					'user_name' => $user_name,
					'social_user_id' => $social_user_account,
					'followers' => $followers
				);

			wpFluent()->table($this->gphoebe_user_social_account)->where('user_id',$user_id)->insert($args);

			$this->responseSuccess(array(
				'status' => 'success'
			));
		}
		
	}

}

?>