<?php
/**
 * Created by PhpStorm.
 * User: rafsan
 * Date: 4/3/18
 * Time: 12:23 AM
 */

namespace Gphoebe\App\Modules\AjaxReq\Backend;

use Gphoebe\App\Modules\AjaxHandler;

class Withdrawl extends AjaxHandler {

	public function __construct() {
		parent::__construct();
	}

	public function getAllWithdrawl()
	{
		$getAllRequest = wpFluent()->table('gphoebe_withdraw_request')
								   ->where('request_status', 'pending')
								   ->join('gphoebe_users', 'gphoebe_users.user_id' ,'=', 'gphoebe_withdraw_request.request_by')
								   ->get();
		$this->responseSuccess(array(
			'status' => 'success',
			'lists' => $getAllRequest
		));

	}

}