<?php

namespace Gphoebe\App\Modules;

use Gphoebe\App;

class DefaultModule
{
    public function handle()
    {
        wp_send_json(App::make('config')->all());
    }
}
