<?php

namespace Gphoebe\App\Modules\PostType;

use Gphoebe\App;

class RegisterPostType {

	public $app;
	public $cpt;

	public function __construct()
	{
		$this->app = App::make();
		$this->cpt = 'gphoebe';
		$this->setRegisterPostType();
	}

	public function setRegisterPostType()
	{
		$labels = array(
			'name'               => __( 'Gphoebe Portal', $this->cpt ),
			'singular_name'      => __( 'Portal', $this->cpt ),
			'menu_name'          => __( 'Gphoebe Portal','admin menu', $this->cpt ),
			'add_new'            => __( 'Add Portal', $this->cpt ),
			'add_new_item'       => __( 'Add New Portal', $this->cpt ),
			'edit'               => __( 'Edit', $this->cpt ),
			'edit_item'          => __( 'Edit Portal', $this->cpt ),
			'new_item'           => __( 'New Portal', $this->cpt ),
			'view'               => __( 'View Portal', $this->cpt ),
			'view_item'          => __( 'View Portal', $this->cpt ),
			'search_items'       => __( 'Search Portal', $this->cpt ),
			'not_found'          => __( 'No Portal Found', $this->cpt ),
			'not_found_in_trash' => __( 'No Portal Found in Trash',
				$this->cpt ),
			'parent'             => __( 'Parent Portal', $this->cpt ),
		);

		$post_type_name = apply_filters('Gphoebe_portal_post_type_name', $this->cpt);
		
		$cpt_slug = apply_filters('Gphoebe_portal_post_type_slug', 'portals');
		
		$args = array(
				'public'             => true,
				'publicly_queryable' => true,
				'show_ui'            => false,
				'show_in_menu'       => false,
				'query_var'          => true,
                'capability_type'    => 'post',
                'has_archive'        => true,
                'hierarchical'       => false,
                'labels'             => $labels,
                'rewrite'            => array( 'slug' => $cpt_slug ),
				'supports' => array('title', 'editor', 'thumbnail', 'excerpt'),
				'menu_icon'          => 'dashicons-screenoptions'
		);
		
		register_post_type( $this->cpt,  $args);

		flush_rewrite_rules();
	}

}