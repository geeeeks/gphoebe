<?php

namespace Gphoebe\App\Providers;

use Gphoebe\Framework\Foundation\Provider;

/**
 * This provider will be loaded only on frontend (public)
 */
class FrontendProvider extends Provider
{
    /**
     * The provider booting method to boot this provider
     */
    public function booting()
    {
        // ...
    }

    /**
     * The provider booted method to be called after booting
     */
    public function booted()
    {
        // ...
    }
}
