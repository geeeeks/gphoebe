<?php

namespace Gphoebe\App\Providers;

use Gphoebe\Framework\Foundation\Provider;

/**
 * This provider will be loaded only on WpFluent (Common)
 */
class WpFluentProvider extends Provider
{
    /**
     * The provider booting method to boot this provider
     */
    public function booting()
    {
        require_once $this->app->appPath().'Services/wpfluent/wpfluent.php';
    }
}
