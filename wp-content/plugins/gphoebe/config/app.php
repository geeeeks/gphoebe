<?php

return [
    'env'       => 'dev',
    'providers' => [
        'core' => [
            'Gphoebe\Framework\Foundation\AppProvider',
            'Gphoebe\Framework\Config\ConfigProvider',
            'Gphoebe\Framework\Request\RequestProvider',
            'Gphoebe\Framework\View\ViewProvider',
        ],

        'plugin' => [
            'common' => [
                'Gphoebe\App\Providers\CommonProvider',
                'Gphoebe\App\Providers\WpFluentProvider',
            ],

            'backend' => [
                'Gphoebe\App\Providers\BackendProvider',
            ],

            'frontend' => [
                'Gphoebe\App\Providers\FrontendProvider',
            ],
        ],
    ],
];
