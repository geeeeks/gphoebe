<?php

namespace Gphoebe\Framework\Request;

use Gphoebe\Framework\Foundation\Provider;

class RequestProvider extends Provider
{
    /**
     * The provider booting method to boot this provider
     */
    public function booting()
    {
        $this->app->bindSingleton('request', function ($app) {
            return new Request($app, $_GET, $_POST, $_FILES);
        }, 'Request', 'Gphoebe\Framework\Request\Request');
    }
}
