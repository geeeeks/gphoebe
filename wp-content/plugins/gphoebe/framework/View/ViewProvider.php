<?php

namespace Gphoebe\Framework\View;

use Gphoebe\Framework\Foundation\Provider;

class ViewProvider extends Provider
{
    /**
     * The provider booting method to boot this provider
     */
    public function booting()
    {
        $this->app->bind('view', function ($app) {
            return new View($app);
        }, 'View', 'Gphoebe\Framework\View\View');
    }
}
