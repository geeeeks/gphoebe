<?php defined('ABSPATH') or die;

/*
Plugin Name: Gphoebe
Description: Gphoebe WordPress Plugin
Version: 1.0.0
Author: Geeeeks
Author URI: geeeeks.com
Plugin URI: 
License: GPLv2
Text Domain: gphoebe
Domain Path: /resources/languages
*/

include 'framework/Foundation/Bootstrap.php';

use Gphoebe\Framework\Foundation\Bootstrap;

Bootstrap::run(__FILE__);
