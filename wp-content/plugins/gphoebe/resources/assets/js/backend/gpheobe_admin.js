import Vue from 'vue';
import router from './route';
import ElementUI from 'element-ui'
import locale from 'element-ui/lib/locale/lang/en'
import 'element-ui/lib/theme-chalk/index.css'

// var pay = require('paypal-pay')
// window.pay = pay({
//     //required parameters
//     'userId': 'rafsanhashemi-facilitator_api1.gmail.com',
//     'password': 'BNFZEUEJSSYWD55X',
//     'signature' : 'AFcWxV21C7fd0v3bYYYRCpSSRl31AxRgCeunTkBXxsGK4-onnPYoyw4R',

//     //make sure that senderEmail and above credentials are from the same paypal account
//     //otherwise paypal won't compete payment automatically
//     'senderEmail' : 'rafsanhashemi-facilitator@gmail.com',

//     //optional parameters and their defaults
//     'sandbox': true,
//     'feesPayer': 'SENDER',
//     'currencyCode': 'USD',
// })



Vue.use(ElementUI, {locale});

Vue.mixin({
    data() {
        return {
            slug: admin_helper.plugin_slug
        }
    },
	methods: {
		msg(type, msg) {
            if(type && msg) {
                this.$message({
                  message: msg,
                  type: type
                });
            }
        },
        notify(type, msg) {
        	if(type && msg) {
                this.$notify({
                  title: type,
                  message: msg,
                  type: type
                });
            }
        }
	}
})

let GphoebeBackend = new Vue({
	router
}).$mount('#gphoebe_backend');
