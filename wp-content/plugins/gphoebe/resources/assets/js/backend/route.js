import Vue from 'vue';
import VueRouter from 'vue-router';

Vue.use(VueRouter);

import BackendView from './Backend.vue'

/**
 * Settings
 */
import Settings from './components/settings/Settings.vue'

/**
 * Category
 */
import Category from './components/Category/Category.vue'

/**
 * Withdraw
 */
import Withdraw from './components/withdraw/withdraw.vue'

/**
 *Refer
 */
import Refer from './components/Referer/refer.vue'

export default new VueRouter({
	routes: [
		{
			path: '/',
			name: 'default',
			component: BackendView,
			children: [
				{
					path: '/gphoebe-category',
					name: 'gphoebe_category',
					component: Category
				},
				{
					path: '/gphoebe-settings',
					name: 'gphoebe_settings',
					component: Settings
				},
				{
					path: '/gphoebe-withdraw',
                    name: 'gphoebe_withdraw',
                    component: Withdraw
				},
				{
					path: '/gphoebe-refer',
                    name: 'gphoebe_refer',
                    component: Refer
				}
			]
		}
	]
})
