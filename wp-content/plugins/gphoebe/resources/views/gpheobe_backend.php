<?php
    if($_SERVER['REQUEST_METHOD'] == "POST") {
        if(array_key_exists('verify_sign', $_REQUEST)) {
            $getId = wpFluent()->table('gphoebe_users')->where('refer_id', $_REQUEST['item_name'])->first()->user_id;
            $updateArgs = array(
                'request_status' => 'paid'
            );

            wpFluent()->table('gphoebe_withdraw_request')->where('request_by', $getId)->update($updateArgs);

            $argsTransaction = array(
                'transaction_id' => $_REQUEST['txn_id'],
                'transaction_type' => 'paypal',
                'amount'            => $_REQUEST['mc_gross'],
                'type'             => 'withdraw',
                'user_by'         => $getId,
                'note'            => 'Withdraw',
                'created_at'      => date( 'Y-m-d H:i:s'),
                'updated_at'     => date( 'Y-m-d H:i:s')
            );

            $transID = wpFluent()->table('gphoebe_transaction')->insert($argsTransaction);

            $argsPayment = array(
                    'transaction_id' => $transID,
                    'user_to'        => $getId,
                    'user_by'        => get_current_user_id(),
                    'real_amount'    => $_REQUEST['mc_gross'] / 0.80,
                    'convert_token'  => $_REQUEST['mc_gross'] / 0.80,
                    'created_at'      => date( 'Y-m-d H:i:s'),
                    'updated_at'     => date( 'Y-m-d H:i:s')
            );

            wpFluent()->table('gphoebe_payment')->insert($argsPayment);

            $getCurrentBalance = wpFluent()->table('gphoebe_user_balance')->where('user_id', $getId)->first()->current_balance;

            $argsBalance = array(
               'current_balance' => $getCurrentBalance - ($_REQUEST['mc_gross'] / 0.80)
            );

            wpFluent()->table('gphoebe_user_balance')->where('user_id', $getId)->update($argsBalance);

        }
    }
?>
<div id="gphoebe_backend">
	<router-view name="default"></router-view>
</div>