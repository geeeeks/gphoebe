<?php

namespace Gphoebe;

use Gphoebe\Framework\Foundation\AppFacade;

class App extends AppFacade
{
    public static $key = 'app';
}
