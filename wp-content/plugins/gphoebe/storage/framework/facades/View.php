<?php

namespace Gphoebe;

use Gphoebe\Framework\Foundation\AppFacade;

class View extends AppFacade
{
	static $key = 'view';
}
