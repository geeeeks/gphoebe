const mix      = require('laravel-mix');
const min = '';

mix.setPublicPath('public');
mix.setResourceRoot('../');

mix.sass('resources/assets/css/gphoebe_backend.scss', `css/gpheobe_backend.css`);
mix.js('resources/assets/js/backend/gpheobe_admin.js', `js/gpheobe_admin.js`);
