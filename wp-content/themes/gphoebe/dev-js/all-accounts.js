var _ = jQuery;
_(document).ready(function() {
    var allAccount = {
        instagramURI: 'https://www.instagram.com/',
        instaInfix: '/?__a=1',
        SI_PREFIXES : ["", "k", "M", "G", "T", "P", "E"],
        fixFollowerObject( { followers } ) {
            var tier = Math.log10(followers) / 3 | 0;
            if(tier == 0) return followers
            var prefix = this.SI_PREFIXES[tier];
            var scale = Math.pow(10, tier * 3);
            var scaled = followers / scale;
            return scaled.toFixed(1) + prefix;
        },
        renderSocialAccountTemplate(social) {
            var html = "";
            if(social.length) {
                social.forEach((item) => {
                    html += "<tr>"+
                        "<td>" + item.user_name + "</td>" +
                        "<td>" + item.fixFollowers + "</td>"+
                        "<td><i class='material-icons'>delete</i></td>" +
                        "</tr>";

                })
            } else {
                html += "<tr>"+
                    "<td></td>" +
                    "<td>No Data Found! Add New Account</td>"+
                    "<td></td>" +
                    "</tr>";
            }


            _('#social-accounts').append(html);


        },
        sendRequestToValidate() {
            var userName = _('.instagram-username').val();
            var modifyUserNameWithURI = this.instagramURI + userName + this.instaInfix;
            _.get(modifyUserNameWithURI)
                .then((response) => {
                    var user = response.graphql.user
                    this.sendStoreRequestAfterValidateUser(user)
                })
                .fail((err) => {
                    console.log(err)
                })
            e.preventDefault();
        },
        sendStoreRequestAfterValidateUser({
                                              id,
                                              username,
                                              profile_pic_url_hd,
                                              edge_followed_by : {
                                                  count
                                              }
                                          }) {
            let details = {
                user_id : id,
                user_name: username,
                dp: profile_pic_url_hd,
                followers: count,
                action: 'gphoebe_store_social_account'
            }

            _.post(user_account.ajaxurl, details)
                .then((response) => {
                    console.log('done')
                })
                .fail((err) => {
                    console.log(err)
                })

        },
        init() {
            _.get(user_account.ajaxurl, {action: 'gphoebe_get_social_account'})
                .then((response) => {
                    response.data.user.forEach((item) => {
                        item.fixFollowers = this.fixFollowerObject(item)
                    })

                    this.renderSocialAccountTemplate(response.data.user)
                })
                .fail((err) => {
                    console.log(err)
                })
        }
    };
    _('#verify-instagram').click((e) => {
        allAccount.sendRequestToValidate();
    });

    allAccount.init();
});