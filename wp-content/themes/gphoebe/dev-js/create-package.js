var _ = jQuery;
_(document).ready(() => {

    /**
     * Create Package class
     * for maintain page-create-package.php
     */

    class createGphoebePackage {
        constructor(slug) {
            this.fetchAction =
            this.storeAction = slug + '_store_packages'
        }

        setValue() {
            this.time = _('.package-time').val()
            this.cost = _('.package-cost').val()
            this.description = _('.package-description').val()
        }

        storePackage() {
            this.setValue()
            const data = {
                action : this.storeAction,
                time: this.time,
                cost: this.cost,
                description: this.description
            }
        }

        fetchPackages() {

            const data = {
                action : this.fetchAction
            }

            _.get(user_account.ajaxurl, data)
                .then(({ data: { packages } }) => {
                    console.log(packages)
                })
                .fail((err) => {
                    console.log(err)
                })
        }

        init() {
            this.fetchPackages()
        }
    }

    this.createGpPackage = new createGphoebePackage(user_account.slug)

    _('.create-package').click((e) => {
        this.createGpPackage.storePackage()
    })

    this.createGpPackage.init()

});