var _ = jQuery;
_(document).ready(() => {
    let integratePaypal = {
        init() {
            paypal.Button.render({
                env: 'sandbox',
                // Pass the client ids to use to create your transaction on sandbox and production environments

                client: {
                    sandbox:    'AQEKVjHurUTu8fthoPhE-8tVmSIpChLjfgC9_gXzZETnRY8A0xu7GhBEPRQDxYB_8ktXFqKErOt5Rhay', // from https://developer.paypal.com/developer/applications/
                    production: 'AQEKVjHurUTu8fthoPhE-8tVmSIpChLjfgC9_gXzZETnRY8A0xu7GhBEPRQDxYB_8ktXFqKErOt5Rhay'  // from https://developer.paypal.com/developer/applications/
                },

                // Pass the payment details for your transaction
                // See https://developer.paypal.com/docs/api/payments/#payment_create for the expected json parameters

                payment: function(data, actions) {
                    return actions.payment.create({
                        transactions: [
                            {
                                amount: {
                                    total: _('.fund-amount').val(),
                                    currency: 'USD'
                                }
                            }
                        ]
                    });
                },

                // Display a "Pay Now" button rather than a "Continue" button

                commit: true,

                // Pass a function to be called when the customer completes the payment

                onAuthorize(data, actions) {
                    // var that = _;
                    return actions.payment.execute().then(function(response) {
                        const data = {
                            pay_id: response.id,
                            payment_type: 'paypal',
                            amount: response.transactions[0].amount.total,
                            buyer_id: user_account.account_id,
                            type: 'add',
                            note: 'Fund Add',
                            action: 'gphoebe_store_transaction_details'
                        }
                        _.post(user_account.ajaxurl, data)
                            .then((res) => {
                                console.log(res)
                            })
                            .fail((err) => {
                                console.log(err)
                            })
                        console.log('The payment was completed!', response);
                    });
                },

                // Pass a function to be called when the customer cancels the payment

                onCancel: function(data) {
                    console.log('The payment was cancelled!');
                }

            }, '#myContainerElement');
        }
    }

    integratePaypal.init();

});