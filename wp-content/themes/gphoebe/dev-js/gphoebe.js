var _ = jQuery;
_(document).ready(() => {

    let gphoebe = {
        SI_PREFIXES : ["", "k", "M", "G", "T", "P", "E"],
        fixFollowerObject( { followers }  ) {
            var tier = Math.log10(followers) / 3 | 0;
            if(tier == 0) return followers
            var prefix = this.SI_PREFIXES[tier];
            var scale = Math.pow(10, tier * 3);
            var scaled = followers / scale;
            return scaled.toFixed(1) + prefix;
        },
        renderTemplate(topUser) {
            let html = "";
            topUser.forEach((item) => {
                html += "<div class='col-sm-6 col-md-4'>" +
                            "<a href='"+ user_account.profile_url + item.user_name +"'>" +
                                "<div class='featured-block clearfix'>" +
                                    "<div class='col-xs-4'>" +
                                        "<img class='thumb img-circle img-responsive' src='" + item.dp +"'>"+
                                    "</div>" +
                                    "<div class='col-xs-8 text-left'>" +
                                        "<p class='title'>"+ item.fix_followers + " followers" +"</p>" +
                                        "<p class='username'><span>@</span>" + item.user_name +"</p>" +
                                        "<p class='price'>"+ '$' + item.price +"</p>" +
                                    "</div>" +
                                "</div>" +
                            "</a>" +
                        "</div>";
            })

            _('.fetaured-users').append(html);
        },
        init() {
            _.get(user_account.ajaxurl , { action: 'gphoebe_get_influencers' })
                .then(( { data: { user } } ) => {
                    user.forEach((item) => {
                        item.fix_followers = this.fixFollowerObject(item)
                    })

                    this.renderTemplate(user)
                })
                .fail((err) => {
                    console.log(err)
                })
        }

    };

    gphoebe.init()

});