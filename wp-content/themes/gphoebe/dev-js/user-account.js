var _ = jQuery;
_(document).ready(function() {
    var userDocument = {
      inputArray: [
        'user_name',
        'phone_number',
        'register_date',
        'languages',
        'countries',
        'age',
        'sex',
        'activity',
        'description'
      ],
      Obj: {
        user_name: '',
        phone_number: '',
        register_date: '',
        languages: '',
        countries: '',
        age: '',
        sex: '',
        activity: '',
        description: ''
      },
        copyReferLink() {
            /* Get the text field */
            var copyText = document.getElementById("refer_link");

            /* Select the text field */
            copyText.select();

            console.log(copyText.select())

            /* Copy the text inside the text field */
            document.execCommand("Copy");

            alert('Copied')

        },
      afterClick(){
        console.log(this);
        this.inputArray.forEach((item) => {
          this.Obj[item] = _('.gp-'+ item).val() || ''
        }) 

        this.Obj.action = 'gphoebe_submit_user_info'; // hook in plugin

        _.post(user_account.ajaxurl, this.Obj)
         .then(function(response) {
          console.log(response);
         })
         .fail(function(err) {
          console.log(response);
         })

      },
      init(){
        _.get(user_account.ajaxurl, { action: 'gphoebe_get_user_profile_info' })
         .then((response) => {
            for(var i in response.data.user_info) {
              _('.gp-' + i).val(response.data.user_info[i])
            }
         })
         .fail((err) => {
            console.log(err)
         })

       const url = 'https://www.instagram.com/' + user_account.my_user_name + '/?__a=1'
            _.get(url)
             .then((response) => {
                let getMedia = response.graphql.user.edge_owner_to_timeline_media.edges

                let mediaArr = []

                getMedia.forEach((item) => {
                   let m = {
                      thumb: item.node.thumbnail_src,
                      shortcode: item.node.shortcode,
                      description: ''
                   }

                   if(item.node.edge_media_to_caption.edges.length > 0) {
                      m.description = item.node.edge_media_to_caption.edges[0].node.text
                   }

                   mediaArr.push(m)
                })

                let html = ""

                mediaArr.forEach((item) => {
                    html += "<tr>"+
                        "<td><img style='height:100px; width: 100px;' src="+ item.thumb +"></td>" +
                        "<td>" + item.description + "</td>"+
                        "<td>" + item.shortcode + "</td>" +
                        "</tr>";
                })

                _('.media-insta').append(html);

                

             })
             .fail((err) => {
                console.log(err)
             })

         const urlr = 'https://www.instagram.com/' + user_account.details.username + '/?__a=1'
          _.get(urlr)
             .then((response) => {
                let getMedia = response.graphql.user.edge_owner_to_timeline_media.edges

                let x = []

               user_account.details.short_code.forEach((i) => {
                  getMedia.forEach((item) => {
                    if(i.sc == item.node.shortcode) {
                      let m = {
                        thumb: item.node.thumbnail_src,
                        shortcode: item.node.shortcode,
                        description: ''
                     }

                     if(item.node.edge_media_to_caption.edges.length > 0) {
                        m.description = item.node.edge_media_to_caption.edges[0].node.text
                     }
                     x.push(m)
                    }
                  })
                })

               let s = ""

               x.forEach((item) => {
                  s += "<div class='col-md-3'>" +
                        "<img src="+ item.thumb +">" +
                        "</div>";
               })

               _('.details-show').append(s);

                console.log('cc',x)

             })
             .fail((err) => {
                console.log(err)
             })
      }

    }
    


    _('.user-account-button').click(function(e) {
        userDocument.afterClick()
    })

    _('.copy-to-clip').click((e) => {
        userDocument.copyReferLink()
    })

    userDocument.init()

});