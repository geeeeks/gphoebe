<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package gphoebe
 */

?>

	</div><!-- #content -->

	<!-- Footer Section Start -->
	<footer>

	    <!-- Copy Right Section Start -->
	    <div class="container">
	        <div class="copyright">
	            <div class="pull-left"><p>&copy; gPhoebe</p></div>
	            <div class="pull-right">
	                <!-- <a href="http://blog.shoutcart.com/" target="_blank">Blog</a> &bull;
	                                    <a href="page/affiliate.html">Affiliates</a> &bull; -->
	                    <a href="<?php _e(home_url('browse')); ?> ">Influencers</a> &bull;
	                                <!-- <a href="http://help.shoutcart.com/" target="_blank">Help</a> &bull; -->
	                <a href="page/terms.html">Terms</a> &bull;
	                <a href="https://gphoebe.com/about-us/">About Us</a> &bull;
	                <a href="https://gphoebe.com/contact">Contact Us</a>
	            </div>
	        </div>
	    </div>
	    <!-- //Copy Right Section End -->

	</footer>
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
