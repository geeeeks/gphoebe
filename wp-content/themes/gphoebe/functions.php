<?php
/**
 * gphoebe functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package gphoebe
 */

if ( ! function_exists( 'gphoebe_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function gphoebe_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on gphoebe, use a find and replace
		 * to change 'gphoebe' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'gphoebe', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Logged Out', 'gphoebe' ),
            'menu-2' => esc_html__('Logged In', 'gphoebe')
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'gphoebe_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'gphoebe_setup' );


if (! function_exists('gphoebe_show_admin_bar')) {
//    function gphoebe_show_admin_bar() {
//	    if (! current_user_can('manage_options')) {
//		    show_admin_bar(false);
//	    } else {
//	        show_admin_bar(true);
//        }
//    }

}

//add_action('set_current_user', 'gphoebe_show_admin_bar');
/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function gphoebe_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'gphoebe_content_width', 640 );
}
add_action( 'after_setup_theme', 'gphoebe_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function gphoebe_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'gphoebe' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'gphoebe' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'gphoebe_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function gphoebe_scripts() {
	wp_enqueue_style( 'gphoebe-style', get_stylesheet_uri() );

	wp_enqueue_script('gphoebe-all-accounts', get_template_directory_uri() . '/js/all-accounts.js', array('jquery'), '20151215', true );

	wp_enqueue_script('gphoebe-user-account', get_template_directory_uri() . '/js/user-account.js', array('jquery'), '20151215', true );

	wp_enqueue_script('gphoebe-main-js', get_template_directory_uri().'/js/gphoebe.js', array('jquery'), '20151215', true);

	wp_enqueue_script('gphoebe-create-packages', get_template_directory_uri().'/js/create-package.js', array('jquery'), '20151215', true);

	wp_enqueue_script('gphoebe-paypal', get_template_directory_uri().'/js/gphoebe-paypal.js', array('jquery'), '20151215', true);

	wp_localize_script( 'gphoebe-user-account', 'user_account', array(
        'ajaxurl' => admin_url( 'admin-ajax.php' ),
        'profile_url' => home_url('/profile/?u='),
        'slug'      => 'gphoebe',
        'account_id' => get_current_user_id(),
        'my_user_name' => get_current_user_id() ? my_details()->user_name : '',
        'details' 	   => array_key_exists('tab', $_REQUEST) ? ($_REQUEST['tab'] == 'details' ? get_shortcodes() : '') : ''
    ) );

	wp_enqueue_script( 'gphoebe-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );

	wp_enqueue_script( 'gphoebe-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'gphoebe_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * WpFluent
 */
require get_template_directory() . '/inc/wpfluent/wpfluent.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

if (! function_exists('check_user_logged_in')) {
    function check_user_logged_in() {
        if (! is_user_logged_in()) {
            wp_redirect(home_url('/login'));
        }
    }
}

function go_to_hell() {
	global  $wp_query;
	$wp_query->set_404();
	status_header( 404 );
	get_template_part( 404 ); exit();
}



if (! function_exists('get_shortcodes')) {
	function get_shortcodes() {
		$getShortCodes  = wpFluent()->table('gphoebe_short_code')->where('user_id', $_REQUEST['inf_id'])->where('order_id', $_REQUEST['order'])->get();

		return array(
			'short_code' => $getShortCodes,
			'username'   => $_REQUEST['inf']
		);
	}
}

if (! function_exists('add_details_shortcode')) {
	function add_details_shortcode() {
		$args = array(
			'user_id' => $_REQUEST['inf_id'],
			'order_id' => $_REQUEST['order'],
			'sc'	   => $_REQUEST['short_code']
		);

		wpFluent()->table('gphoebe_short_code')->insert($args);

		return true;
	}
}



if (! function_exists('create_package')) {
    function create_package() {
        if (array_key_exists('create_package', $_REQUEST)) {
            $getUserId = get_current_user_id();

            $duration = floatval( $_REQUEST['duration'] );

            $description = sanitize_text_field( $_REQUEST['description'] );

            $price = floatval( $_REQUEST['price'] );

            $display = intval($_REQUEST['display']);

            $getAllPackageOfThisUser = wpFluent()->table('gphoebe_pricing')->where('user_id', $getUserId)->get();

            if ($getAllPackageOfThisUser && $display == 1) {
                $args = array(
                  'display' => 0
                );
                wpFluent()->table('gphoebe_pricing')->where('user_id', $getUserId)->update($args);
            }

	        $argsNewPackageOrFirstPackage = array(
		        'user_id' => $getUserId,
		        'duration' => $duration,
		        'description' => $description,
		        'price' => $price
	        );

            if (! $getAllPackageOfThisUser) {
                $argsNewPackageOrFirstPackage['display'] = 1;

                wpFluent()->table('gphoebe_pricing')->insert($argsNewPackageOrFirstPackage);

                return true;
            }

	        $argsNewPackageOrFirstPackage['display'] = $display;
            wpFluent()->table('gphoebe_pricing')->insert($argsNewPackageOrFirstPackage);

            return true;
        }
    }
}

if (! function_exists('get_all_packages')) {
    function get_all_packages() {
        $getUserId = get_current_user_id();

        $getAllPackages = wpFluent()->table('gphoebe_pricing')->where('user_id', $getUserId)
                                                              ->get();

        return $getAllPackages;
    }
}

if (! function_exists('remove_package_by_id')) {
    function remove_package_by_id() {
        $getUserId = get_current_user_id();

        $getPackageId = intval( $_REQUEST['package_id'] );

        wpFluent()->table('gphoebe_pricing')->where('user_id', $getUserId)
                                            ->where('id', $getPackageId)
                                            ->delete();

        return true;
    }
}

if (! function_exists('my_current_balance')) {
    function my_current_balance() {

        $userId = get_current_user_id();

        $getCurrentBalance = wpFluent()->table('gphoebe_user_balance')
                                        ->where('user_id', $userId)
                                        ->first();
        return $getCurrentBalance;
    }
}

if (! function_exists('my_followers')) {
    function my_followers() {
        $userId = get_current_user_id();

        $isBuyer = wpFluent()->table('gphoebe_users')->where('user_id', $userId)->first();

        if (! $isBuyer->is_buyer) {
            $followers = wpFluent()->table('gphoebe_user_social_account')
                                   ->where('user_id', $userId)
                                   ->first();

            return $followers->followers;
        }

        return $isBuyer->is_buyer;
    }
}

if (! function_exists('my_invitation')) {
    function my_invitation() {
	    $userId = get_current_user_id();

	    $invitations = wpFluent()->table('gphoebe_invitation')->where('influencer_id', $userId)
                                                              ->where('status', 'pending');

	    return array(
		    'count' => $invitations->count(),
		    'invitations' => $invitations->get()
	    );
    }

}

if (! function_exists('my_buying_orders')) {
    function my_buying_orders() {
	    $userId = get_current_user_id();

	    $buyerOrders = wpFluent()->table('gphoebe_order_details')
	                                     ->where('buyer_id', $userId);
	    return array(
		    'count'   => $buyerOrders->count(),
		    'all_orders' => $buyerOrders->get()
	    );
    }
}

if (! function_exists('my_all_orders')) {
    function my_all_orders() {
        $userId = get_current_user_id();

        $influencerAllOrders = wpFluent()->table('gphoebe_order_details')
                                         ->where('influencer_id', $userId);

        return array(
          'count'   => $influencerAllOrders->count(),
          'all_orders' => $influencerAllOrders->get()
        );
    }
}

if (! function_exists('buyer_details')) {
    function buyer_details($id) {
        $buyerDetails = wpFluent()->table('gphoebe_users')->where('user_id' , $id)
                                                          ->first();

        return $buyerDetails;
    }
}

if (! function_exists('my_details')) {
    function my_details(){
        $myId = get_current_user_id();

        $myDetails = wpFluent()->table('gphoebe_users')->where('user_id', $myId)
                                                       ->first();

        return $myDetails;
    }
}

if (! function_exists('inf_order_status')) {
    function inf_order_status(
            $order_details_id,
            $influencer_id ,
            $status)
    {
        if ($status == 'accept' || $status == 'reject') {
	        $argsOrderDetails = array(
		        'order_status'    => $status == 'accept' ? 'pending' : 'cancelled'
	        );

	        wpFluent()->table('gphoebe_order_details')->where('id', $order_details_id)->update($argsOrderDetails);

	        $argsInvitation = array(
		        'status' => $status == 'accept' ? 'accepted' : 'reject'
	        );

	        wpFluent()->table('gphoebe_invitation')->where('order_details_id', $order_details_id)
	                  ->where('influencer_id', $influencer_id)
	                  ->update($argsInvitation);
        }

        if ($status == 'delivered' || $status == 'cancelled') {
	        $argsOrderDetails = array(
		        'order_status'    => $status
	        );

	        wpFluent()->table('gphoebe_order_details')->where('influencer_id', $influencer_id)->where('id', $order_details_id)->update($argsOrderDetails);
        }

        if ($status == 'completed' || $status == 'revision') {
	        $argsOrderDetails = array(
		        'order_status'    => $status
	        );

	        wpFluent()->table('gphoebe_order_details')->where('buyer_id', intval($_REQUEST['buyer_id']))->where('id', $order_details_id)->update($argsOrderDetails);
	        
	        if ($status == 'completed') {
	            $projectAmount = wpFluent()->table('gphoebe_order_details')
                                           ->where('id',$order_details_id)->first()->order_amount;

	            $getBuyerAccount = wpFluent()->table('gphoebe_user_balance')
                                             ->where('user_id', intval($_REQUEST['buyer_id']))->first()->current_balance;

	            $argsBuyerAccountUpdate = array(
	              'current_balance' => $getBuyerAccount - $projectAmount > 0 ? $getBuyerAccount - $projectAmount : 0
                );

	            wpFluent()->table('gphoebe_user_balance')->where('user_id', intval($_REQUEST['buyer_id']))->update($argsBuyerAccountUpdate);

	            $getInfluencerAccount = wpFluent()->table('gphoebe_user_balance')->where('user_id', $influencer_id)->first()->current_balance;

	            $argsInfluencerAccountUpdate = array(
	                    'current_balance' => $getInfluencerAccount + $projectAmount
                );

		        wpFluent()->table('gphoebe_user_balance')->where('user_id', $influencer_id)->update($argsInfluencerAccountUpdate);

		        $argsOrderDetailsUpdate = array(
		          'payment_status' => 'paid'
                );

		        wpFluent()->table('gphoebe_order_details')->where('id', $order_details_id)->update($argsOrderDetailsUpdate);
	        }
        }

    }
}

if (! function_exists('my_total_earnings')) {
    function my_total_earnings() {
        $getMyId = get_current_user_id();

        $myAllOrdersList = wpFluent()->table('gphoebe_order_details')
                                     ->where('order_status', 'completed')
                                     ->where('payment_status', 'paid')
                                     ->where('influencer_id', $getMyId)
                                     ->get();

        $totalEarnings = 0;

        foreach ($myAllOrdersList as $key => $detail) {
            $totalEarnings += $detail->order_amount;
        }

        return $totalEarnings;
    }
}

if (! function_exists('my_total_spent')) {
    function my_total_spent() {
	    $getMyId = get_current_user_id();

	    $myAllOrdersList = wpFluent()->table('gphoebe_order_details')
	                                 ->where('order_status', 'completed')
	                                 ->where('payment_status', 'paid')
	                                 ->where('buyer_id', $getMyId)
	                                 ->get();

	    $totalSpent = 0;

	    foreach ($myAllOrdersList as $key => $detail) {
		    $totalSpent += $detail->order_amount;
	    }

	    return $totalSpent;
    }
}

if (! function_exists('my_referral_link')) {
    function my_referral_link($id) {
        return home_url('/register/?refer='.$id);
    }
}

if (! function_exists('withdraw_front_check')) {
    function withdraw_front_check() {
        $getUserId = get_current_user_id();

        $currentBalance = wpFluent()->table('gphoebe_user_balance')->where('user_id', $getUserId)->first()->current_balance;

        //return $currentBalance;

        return $currentBalance >= get_option('gphoebe_settings')['minimum_withdraw_amount'] ? '' : 'dis-link';
    }
}

if (! function_exists('make_status_class')) {
    function make_status_class($status) {
        $classes = array(
                'completed' => 'alert alert-success',
                'delivered' => 'alert alert-warning',
                'invited'   => 'alert alert-info',
                'pending'   => 'alert alert-warning',
                'revision'  => 'alert alert-warning',
                'cancelled' => 'alert alert-danger',
                'accepted'  => 'alert alert-success',
                'reject'   => 'alert alert-danger',
                'paid'     => 'alert alert-success'
        );

        return $classes[$status];
    }
}

if (! function_exists('add_order_invitation')) {
    function add_order_invitation() {
        $uniqueOrderId = '#' . substr(str_shuffle(str_repeat("0123456789abcdefghijklmnopqrstuvwxyz", 6)), 0, 6);

        $buyerId = get_current_user_id();

        $totalAmount = 0;

        $totalOrder = 0;

        if (array_key_exists('cart', $_SESSION)) {
	        foreach (cart_show() as $value) {
	            $totalOrder++;
	            $totalAmount += $value['price'];
            }
        } else {
	        return;
        }

        $getBalance = wpFluent()->table('gphoebe_user_balance')->where('user_id', $buyerId)->first()->current_balance;

        if ($getBalance < $totalAmount) {
        	return 101;

        }


	    $createdAt = date( 'Y-m-d H:i:s');
        $updatedAt = date( 'Y-m-d H:i:s');

        $argsOrder = array(
                'order_id' => $uniqueOrderId,
                'buyer_id' => $buyerId,
                'total_amount' => $totalAmount,
                'total_order'  => $totalOrder,
                'created_at'   => $createdAt,
                'updated_at'   => $updatedAt
        );

        $orderId = wpFluent()->table('gphoebe_orders')->insert($argsOrder);

	    if (array_key_exists('cart', $_SESSION)) {
		    foreach (cart_show() as $value) {
			    $argsOrderDetails = array(
			        'order_id' => $orderId,
                    'order_description' => $_REQUEST['description'],
                    'order_start_at' => $createdAt,
                    'order_end_at'   => $updatedAt,
                    'order_amount'   => $value['price'],
                    'influencer_id' => $value['user_id'],
                    'buyer_id'      => $buyerId,
			        'created_at'   => $createdAt,
			        'updated_at'   => $updatedAt
                );

			    $orderDetailsId = wpFluent()->table('gphoebe_order_details')->insert($argsOrderDetails);

			    $argsInvitation = array(
			            'buyer_id' => $buyerId,
                        'influencer_id' => $value['user_id'],
                        'order_id'      => $orderId,
                        'order_details_id'  => $orderDetailsId,
			            'created_at'   => $createdAt,
			            'updated_at'   => $updatedAt
                );

			    wpFluent()->table('gphoebe_invitation')->insert($argsInvitation);

			    unset($_SESSION['cart']);

			    return 200;
		    }
	    } else {
		    return;
	    }
    }
}

if (! function_exists('get_all_transactions')) {
	function get_all_transactions() {
		$getMyId = get_current_user_id();

		$getAllTransactions = wpFluent()->table('gphoebe_transaction')->where('user_by', $getMyId)
		                                ->orderBy('created_at', 'DESC')
		                                ->get();
		$transactionsList = array();

		foreach ($getAllTransactions as $trans) {
			$label = "You " . $trans->type . " $ " . $trans->amount . " by " . $trans->transaction_type;
			array_push($transactionsList, $label);
		}

		return $transactionsList;
	}
}

if (! function_exists('send_withdraw_paypal_request')) {
    function send_withdraw_paypal_request() {
        $requestBy = get_current_user_id();

        $requestMethod = $_REQUEST['request_method'];

        $requestAmount = floatval($_REQUEST['request_amount']);

        $requestEmail = $_REQUEST['request_email'];

	    $createdAt = date( 'Y-m-d H:i:s');
	    $updatedAt = date( 'Y-m-d H:i:s');

	    $argsPaypalWithdrawRequest = array(
	            'request_by'     => $requestBy,
                'request_method' => $requestMethod,
                'request_amount' => $requestAmount,
                'request_email'  => $requestEmail,
                'created_at'    => $createdAt,
                'updated_at'    => $updatedAt
        );

	    wpFluent()->table('gphoebe_withdraw_request')->insert($argsPaypalWithdrawRequest);

	    return true;
    }
}

if (! function_exists('cart_add')) {
    function cart_add() {
        if (array_key_exists('influencer', $_REQUEST)) {
	        $influencerId = sanitize_text_field($_REQUEST['influencer']);
        } else {
            go_to_hell();
        }

        if (array_key_exists('price', $_REQUEST)) {
            $price = floatval($_REQUEST['price']);
        } else {
            go_to_hell();
        }

        $_SESSION['cart'][] = array(
                'influencer' => $influencerId,
                'price'     => $price
        );

    }
}

if (! function_exists('cart_show')) {
    function cart_show() {
        $influencerCart = array();

        if (array_key_exists('cart', $_SESSION)) {
	        foreach ($_SESSION['cart'] as $key => $item) {
		        $influencerUserId = $item['influencer'];
		        $influencerName = wpFluent()->table('gphoebe_user_social_account')
		                                    ->select(array('user_name', 'dp'))
		                                    ->where('user_id', $influencerUserId)
		                                    ->first();
		        $modifiedItem = array(
			        'key'       => $key,
			        'user_id'   => $influencerUserId,
			        'user_name' => $influencerName->user_name,
			        'price'     => $item['price'],
			        'dp'        => $influencerName->dp
		        );

		        $influencerCart[] = $modifiedItem;
	        }
        }

        return (object) $influencerCart;
    }
}

if (! function_exists('cart_remove_by_key')) {
    function cart_remove_by_key($key) {
        unset($_SESSION['cart'][$key]);
    }
}

if (! function_exists('registration_extra_data')) {
    function registration_extra_data($user_id, $user_name) {
        $argsUsers = array(
            'user_id' => $user_id,
            'user_name' => $user_name,
            'registration_date' => date( 'Y-m-d H:i:s'),
            'is_buyer'      => '1',
            'refer_id' => substr(str_shuffle(str_repeat("0123456789abcdefghijklmnopqrstuvwxyz", 8)), 0, 8)
        );

        wpFluent()->table('gphoebe_users')->insert($argsUsers);

        $argsBalance = array(
            'user_id' => $user_id
        );

        wpFluent()->table('gphoebe_user_balance')->insert($argsBalance);
    }
}

if (! function_exists('had_referral_code')) {
    function had_referral_code() {
        if (array_key_exists( 'refer_id', $_REQUEST)) {
	        $refer_id = sanitize_text_field( $_REQUEST['refer_id'] );

	        $getUserByReferId = wpFluent()->table('gphoebe_users')->where('refer_id', $refer_id)
                                                                  ->first()->user_id;

	        $referAmount = get_option('gphoebe_settings')['refer_pay_amount'];

	        $getReferUserCurrentBalance = wpFluent()->table('gphoebe_user_balance')
                                                    ->where('user_id', $getUserByReferId)
                                                    ->first()->current_balance;

	        $updateBalance = array(
	                'current_balance' => $getReferUserCurrentBalance + $referAmount
            );

	        wpFluent()->table('gphoebe_user_balance')->where('user_id', $getUserByReferId)
                                                     ->update($updateBalance);


        }
    }
}

if (! function_exists( 'register_nonce_check')) {
    function register_nonce_check() {
        if (array_key_exists('_register_nonce', $_REQUEST)) {
	        $getMyRegisterNonce = sanitize_text_field( $_REQUEST['_register_nonce'] );
	        $getRequestId = intval($_REQUEST['request']);

	        return wp_verify_nonce($getMyRegisterNonce, 'my-login' . $getRequestId);

        }

        return false;
    }
}


if (! function_exists('browse_paginate')) {
    function browse_paginate($page) {
        if(array_key_exists('c_p', $_REQUEST)) {
            $_REQUEST['c_p'] = $page;
        }
        return _e( home_url('/browser/?') . http_build_query($_REQUEST));
    }
}

if (! function_exists('browse')) {
    function browse() {
	    $getRequest = $_REQUEST;

	    $currentPage = array_key_exists('c_p', $getRequest) ? intval($getRequest['c_p']) : 1;

	    $perPage = 4;

	    $skip = $perPage * ($currentPage - 1);

        $query = wpFluent()->table('gphoebe_user_social_account')
                            ->select(
                                    array(
                                            'gphoebe_user_social_account.user_name',
                                            'gphoebe_user_social_account.dp',
                                            'gphoebe_user_social_account.user_id'

                                    )
                            )
                           ->join('gphoebe_users', 'gphoebe_users.user_id', '=', 'gphoebe_user_social_account.user_id')
                           ->join('ghoebe_user_category', 'ghoebe_user_category.user_id', '=', 'gphoebe_user_social_account.user_id')

        ;

        if ($getRequest) {

            if (array_key_exists('c', $getRequest)) {
	            $s = explode(',', $getRequest['c']);
                $query = $query->whereIn('ghoebe_user_category.category_id', $s);
            }

            if (array_key_exists('lan', $getRequest)) {
                $query = $query->where('gphoebe_users.language',sanitize_text_field($getRequest['lan']));

                //dd($query->get());
            }

            if (array_key_exists('sex', $getRequest)) {
                $query = $query->where('gphoebe_users.sex', sanitize_text_field($getRequest['sex']));
            }

            if (array_key_exists('country', $getRequest)) {
                $query = $query->where('gphoebe_users.country', sanitize_text_field($getRequest['country']));
            }

            if (array_key_exists('search', $getRequest)) {
                $value = $getRequest["search"];
                $query = $query->where('gphoebe_user_social_account.user_name', 'LIKE','%'. $value .'%')
                               ->orWhere('gphoebe_users.country', 'LIKE', '%'. $value .'%')
                               ->orWhere('gphoebe_users.sex', 'LIKE', '%'. $value .'%')
                               ->orWhere('gphoebe_users.description', 'LIKE', '%'. $value .'%');
	        }

	        $data = $query->limit($perPage)->offset($skip)->get();

	        $dataCount = count($data);

	        $from = $dataCount > 0 ? ($currentPage - 1) * $perPage + 1 : null;

	        $to = $dataCount > 0 ? $from + $dataCount - 1 : null;

	        $total = $query->count();

	        $lastPage = (int) ceil($total / $perPage);

	        return  array(
		        'current_page'  => $currentPage,
		        'per_page'      => $perPage,
		        'from'          => $from,
		        'to'            => $to,
		        'last_page'     => $lastPage,
		        'total'         => $total,
		        'data'          => $data,
	        );

        } else {
	        $data = $query->get();

	        $dataCount = count($data);

	        $from = $dataCount > 0 ? ($currentPage - 1) * $perPage + 1 : null;

	        $to = $dataCount > 0 ? $from + $dataCount - 1 : null;

	        $total = $query->count();

	        $lastPage = (int) ceil($total / $perPage);

	        return array(
		        'current_page'  => $currentPage,
		        'per_page'      => $perPage,
		        'from'          => $from,
		        'to'            => $to,
		        'last_page'     => $lastPage,
		        'total'         => $total,
		        'data'          => $data,
	        );
        }
    }
}

if (! function_exists( 'influencer' )) {
    function influencer() {
	    global $wp_query;
	    $getUserName = sanitize_text_field( $_REQUEST['u'] );

	    if (! $getUserName ) {
		    go_to_hell();
	    }

	    $getInfluencerDetails = wpFluent()->table('gphoebe_user_social_account')
	                                      ->where('user_name', $getUserName)
	                                      ->first();

	    if (! $getInfluencerDetails ) {
		    go_to_hell();
	    }

	    $getInfluencerDetails->pricing = wpFluent()->table('gphoebe_pricing')
	                                               ->where('user_id', $getInfluencerDetails->user_id)
	                                               ->get();

	    $getInfluencerDetails->details = wpFluent()->table('gphoebe_users')
	                                               ->where('user_id', $getInfluencerDetails->user_id)
	                                               ->first();

	    $getInfluencerDetails->category = wpFluent()->table('ghoebe_user_category')
	                                                ->join('gphoebe_category',
		                                                'gphoebe_category.id',
		                                                '=',
		                                                'ghoebe_user_category.category_id'
	                                                )
	                                                ->where('user_id', $getInfluencerDetails->user_id)
	                                                ->first();

	    $getInfluencerDetails->is_fav = wpFluent()->table('gphoebe_favorities')
	                                              ->where('user_id', get_current_user_id())
	                                              ->where('fav_id', $getInfluencerDetails->user_id)
	                                              ->first() ? true : false;


	    return $getInfluencerDetails;
    }
}


/**
 * Check Route for side bar
 */
if(! function_exists('route_checking') ) {
    function route_checking($current_url) {
        return get_permalink() == home_url($current_url) ? _e('active') : '';
    }
}

/**
 * Check Route and modify balance route
 */
if(! function_exists('bank_route')) {
    function bank_route($tab) {
        return _e( home_url('/bank/?type=' .sanitize_text_field($_GET['type']) .'&tab=' . $tab) );
    }
}

/**
 * Check Balance Tab
 */
if(! function_exists('bank_tab_checking')) {
    function bank_tab_checking($tab) {
        return sanitize_text_field( $_GET['tab'] ) == $tab ? _e('active') : '';
    }
}

if (! function_exists('bank_tab_content')) {
    function bank_tab_content($tab) {
	    return sanitize_text_field( $_GET['tab'] ) == $tab ? true : false;
    }
}

/**
 * Check user profile tab to active
 */
if(! function_exists('user_profile_tab')) {
    function user_profile_tab($tab) {
        return sanitize_text_field( $_GET['tab'] ) == $tab ? _e('active') : '';
    }
}

/**
 * add user profile tab route
 */
if(! function_exists('user_profile_tab_route') ) {
    function user_profile_tab_route($tab) {
        return _e( home_url( '/user-profile/?tab=' . $tab ) );
    }
}

/**
 * Logically Handle user profile content
 */
if(! function_exists('user_profile_content')) {
	function user_profile_content($tab) {
		return sanitize_text_field( $_GET['tab'] ) == $tab ? true : false;
	}
}

/**
 * Influencer Route Check
 */
if(! function_exists('influencer_section_route_active')) {
    function influencer_section_route_active($tab) {
	    return sanitize_text_field( $_GET['tab'] ) == $tab ? _e('active') : '';
    }
}

/**
 * Influencer Route link Generate
 */
if(! function_exists('influencer_section_route_gen') ) {
    function influencer_section_route_gen($tab) {
        return _e(home_url('/influencer-section/?tab='. $tab));
    }
}

/**
 * Logically handle influencer tab content
 */
if(! function_exists('influencer_section_tab_content')) {
    function influencer_section_tab_content($tab) {
        return sanitize_text_field( $_GET['tab'] ) == $tab ? true : false;
    }
}

/**
 * Business Route Check
 */

if(! function_exists('business_section_route_active')) {
	function business_section_route_active($tab) {
		return sanitize_text_field( $_GET['tab'] ) == $tab ? _e('active') : '';
	}
}

/**
 * Business Route link Generate
 */
if(! function_exists('business_section_route_gen') ) {
    function business_section_route_gen($tab) {
        return _e(home_url('/business-section/?tab='. $tab));
    }
}


/**
 * Logically handle business tab content
 */
if(! function_exists('business_section_tab_content')) {
    function business_section_tab_content($tab) {
        return sanitize_text_field( $_GET['tab'] ) == $tab ? true : false;
    }
}


if(! function_exists('create_package_route_active')) {
	function create_package_route_active($tab) {
		return sanitize_text_field( $_GET['tab'] ) == $tab ? _e('active') : '';
	}
}

/**
 * Create Package Route link Generate
 */
if(! function_exists('create_package_route_gen') ) {
    function create_package_route_gen($tab) {
        return _e(home_url('/create-package/?tab='. $tab));
    }
}

/**
 * Logically handle packages tab content
 */
if(! function_exists('create_package_content')) {
    function create_package_content($tab) {
        return sanitize_text_field( $_GET['tab'] ) == $tab ? true : false;
    }
}



/**
 * Logically Handle user cart content
 */
if(! function_exists('cart')) {
    function cart($tab) {
        return sanitize_text_field( $_GET['tab'] ) == $tab ? true : false;
    }
}

if (! function_exists('top18UserUrl')) {
    function top18UserUrl($userName) {
        return _e(home_url('/profile/?u=' . $userName));
    }
}

if (! function_exists('gphoebeurl')) {
    function gphoebeurl() {
	    return (object) array(
		    'home' => home_url(),
		    'profile' => home_url('/profile'),
		    'login'   => home_url('/login/'),
		    'register'  => home_url('/regiserter')
	    );
    }

}

/**
 * Frontend Dashboard Sidebar
 */
if(! function_exists('gphoebe_sidebar')) {
	function gphoebe_sidebar()
	{
		?>
		<div class="sidebar" data-color="purple" data-image="../assets/img/sidebar-1.jpg">
            <!--
        Tip 1: You can change the color of the sidebar using: data-color="purple | blue | green | orange | red"

        Tip 2: you can also add an image using data-image tag
    -->
            <!-- <div class="logo">
                <a href="http://www.creative-tim.com" class="simple-text">
                    Creative Tim
                </a>
            </div> -->
            <br>
            <div class="sidebar-wrapper">
                <ul class="nav">
                	<li style="height: 40px;">
                        <a href="<?php _e(home_url('/')); ?>">
                            <i class="fa fa-home"></i>
                            <p>Home</p>
                        </a>
                    </li>
                    <li  style="height: 40px;" class="<?php route_checking('/my-account/'); ?>">
                        <a href="<?php _e(home_url('my-account')); ?>" >
                            <i class="fa fa-columns"></i>
                            <p>Dashboard</p>
                        </a>
                    </li>
                    <li  style="height: 40px;" class="<?php route_checking('/browser/') ?>">
                        <a href=" <?php _e(home_url('browse')); ?>">
                            <i class="fa fa-search"></i>
                            <p>Browse</p>
                        </a>
                    </li>
                    <li  style="height: 40px;" class="<?php route_checking('/cart/') ?>">
                        <a href=" <?php _e(home_url('cart')); ?>">
                            <i class="fa fa-shopping-cart"></i>
                            <p>Cart</p>
                        </a>
                    </li>
                    <li  style="height: 40px;" class="<?php route_checking('/favorites/') ?>">
                        <a href=" <?php _e(home_url('favorites')); ?>">
                            <i class="fa fa-star"></i>
                            <p>Favorite</p>
                        </a>
                    </li>                    
                    <li  style="height: 40px;" class="<?php route_checking('/user-profile/'); ?>">
                        <a href="<?php _e(home_url('user-profile/?tab=profile_information')); ?>" >
                            <i class="fa fa-user"></i>
                            <p>User Profile</p>
                        </a>
                    </li>
                    <li  style="height: 40px;" class="<?php route_checking('/influencer-section/') ?>">
                        <a href="<?php _e(home_url('influencer-section/?tab=all_accounts')); ?>" >
                            <i class="fa fa-users"></i>
                            <p>Influencer</p>
                        </a>
                    </li>
                    <li  class="<?php route_checking('/business-section/') ?>">
                        <a href=" <?php _e(home_url('business-section/?tab=all_selling_orders')); ?>" >
                            <i class="fa fa-briefcase"></i>
                            <p>Business</p>
                        </a>
                    </li>

                    <li  class="<?php route_checking('/bank/') ?>">
                        <a href=" <?php _e(home_url('bank/?type=&tab=')); ?>">
                            <i class="fa fa-dollar"></i>
                            <p>Bank</p>
                        </a>
                    </li>
                    <?php if (! my_details()->is_buyer): ?>
                        <li  style="height: 40px;" class="<?php route_checking('/create-package/') ?>">
                            <a href="<?php _e(home_url('create-package/?tab=all_packages')); ?>" >
                                <i class="fa fa-instagram"></i>
                                <p>Create Package</p>
                            </a>
                        </li>
                    <?php endif; ?>
                    <li>
                    	<a href="<?php echo wp_logout_url( get_home_url() ); ?>" >
                    		<i class="fa fa-sign-out"></i>
                    		<p>Logout</p>
                    </li>
                    
                    <!-- <li  style="height: 40px;" class="active-pro">
                        <a href="upgrade.html">
                            <i class="material-icons">unarchive</i>
                            <p>Upgrade to PRO</p>
                        </a>
                    </li> -->
                </ul>
            </div>
        </div>
		<?php
	}
}

/**
 * make well documented and use function exists
 * @return array
 */

// All languages

function languages()
{
	return array(
        'en' => 'English' , 
        'aa' => 'Afar' , 
        'ab' => 'Abkhazian' , 
        'af' => 'Afrikaans' , 
        'am' => 'Amharic' , 
        'ar' => 'Arabic' , 
        'as' => 'Assamese' , 
        'ay' => 'Aymara' , 
        'az' => 'Azerbaijani' , 
        'ba' => 'Bashkir' , 
        'be' => 'Byelorussian' , 
        'bg' => 'Bulgarian' , 
        'bh' => 'Bihari' , 
        'bi' => 'Bislama' , 
        'bn' => 'Bengali/Bangla' , 
        'bo' => 'Tibetan' , 
        'br' => 'Breton' , 
        'ca' => 'Catalan' , 
        'co' => 'Corsican' , 
        'cs' => 'Czech' , 
        'cy' => 'Welsh' , 
        'da' => 'Danish' , 
        'de' => 'German' , 
        'dz' => 'Bhutani' , 
        'el' => 'Greek' , 
        'eo' => 'Esperanto' , 
        'es' => 'Spanish' , 
        'et' => 'Estonian' , 
        'eu' => 'Basque' , 
        'fa' => 'Persian' , 
        'fi' => 'Finnish' , 
        'fj' => 'Fiji' , 
        'fo' => 'Faeroese' , 
        'fr' => 'French' , 
        'fy' => 'Frisian' , 
        'ga' => 'Irish' , 
        'gd' => 'Scots/Gaelic' , 
        'gl' => 'Galician' , 
        'gn' => 'Guarani' , 
        'gu' => 'Gujarati' , 
        'ha' => 'Hausa' , 
        'hi' => 'Hindi' , 
        'hr' => 'Croatian' , 
        'hu' => 'Hungarian' , 
        'hy' => 'Armenian' , 
        'ia' => 'Interlingua' , 
        'ie' => 'Interlingue' , 
        'ik' => 'Inupiak' , 
        'in' => 'Indonesian' , 
        'is' => 'Icelandic' , 
        'it' => 'Italian' , 
        'iw' => 'Hebrew' , 
        'ja' => 'Japanese' , 
        'ji' => 'Yiddish' , 
        'jw' => 'Javanese' , 
        'ka' => 'Georgian' , 
        'kk' => 'Kazakh' , 
        'kl' => 'Greenlandic' , 
        'km' => 'Cambodian' , 
        'kn' => 'Kannada' , 
        'ko' => 'Korean' , 
        'ks' => 'Kashmiri' , 
        'ku' => 'Kurdish' , 
        'ky' => 'Kirghiz' , 
        'la' => 'Latin' , 
        'ln' => 'Lingala' , 
        'lo' => 'Laothian' , 
        'lt' => 'Lithuanian' , 
        'lv' => 'Latvian/Lettish' , 
        'mg' => 'Malagasy' , 
        'mi' => 'Maori' , 
        'mk' => 'Macedonian' , 
        'ml' => 'Malayalam' , 
        'mn' => 'Mongolian' , 
        'mo' => 'Moldavian' , 
        'mr' => 'Marathi' , 
        'ms' => 'Malay' , 
        'mt' => 'Maltese' , 
        'my' => 'Burmese' , 
        'na' => 'Nauru' , 
        'ne' => 'Nepali' , 
        'nl' => 'Dutch' , 
        'no' => 'Norwegian' , 
        'oc' => 'Occitan' , 
        'om' => '(Afan)/Oromoor/Oriya' , 
        'pa' => 'Punjabi' , 
        'pl' => 'Polish' , 
        'ps' => 'Pashto/Pushto' , 
        'pt' => 'Portuguese' , 
        'qu' => 'Quechua' , 
        'rm' => 'Rhaeto-Romance' , 
        'rn' => 'Kirundi' , 
        'ro' => 'Romanian' , 
        'ru' => 'Russian' , 
        'rw' => 'Kinyarwanda' , 
        'sa' => 'Sanskrit' , 
        'sd' => 'Sindhi' , 
        'sg' => 'Sangro' , 
        'sh' => 'Serbo-Croatian' , 
        'si' => 'Singhalese' , 
        'sk' => 'Slovak' , 
        'sl' => 'Slovenian' , 
        'sm' => 'Samoan' , 
        'sn' => 'Shona' , 
        'so' => 'Somali' , 
        'sq' => 'Albanian' , 
        'sr' => 'Serbian' , 
        'ss' => 'Siswati' , 
        'st' => 'Sesotho' , 
        'su' => 'Sundanese' , 
        'sv' => 'Swedish' , 
        'sw' => 'Swahili' , 
        'ta' => 'Tamil' , 
        'te' => 'Tegulu' , 
        'tg' => 'Tajik' , 
        'th' => 'Thai' , 
        'ti' => 'Tigrinya' , 
        'tk' => 'Turkmen' , 
        'tl' => 'Tagalog' , 
        'tn' => 'Setswana' , 
        'to' => 'Tonga' , 
        'tr' => 'Turkish' , 
        'ts' => 'Tsonga' , 
        'tt' => 'Tatar' , 
        'tw' => 'Twi' , 
        'uk' => 'Ukrainian' , 
        'ur' => 'Urdu' , 
        'uz' => 'Uzbek' , 
        'vi' => 'Vietnamese' , 
        'vo' => 'Volapuk' , 
        'wo' => 'Wolof' , 
        'xh' => 'Xhosa' , 
        'yo' => 'Yoruba' , 
        'zh' => 'Chinese' , 
        'zu' => 'Zulu' , 
        );

}

/**
 * make well documented and use function exists
 * @return array
 */

// All Countries

function countries()
{
	return array(
		'AF' => 'Afghanistan',
		'AX' => 'Aland Islands',
		'AL' => 'Albania',
		'DZ' => 'Algeria',
		'AS' => 'American Samoa',
		'AD' => 'Andorra',
		'AO' => 'Angola',
		'AI' => 'Anguilla',
		'AQ' => 'Antarctica',
		'AG' => 'Antigua And Barbuda',
		'AR' => 'Argentina',
		'AM' => 'Armenia',
		'AW' => 'Aruba',
		'AU' => 'Australia',
		'AT' => 'Austria',
		'AZ' => 'Azerbaijan',
		'BS' => 'Bahamas',
		'BH' => 'Bahrain',
		'BD' => 'Bangladesh',
		'BB' => 'Barbados',
		'BY' => 'Belarus',
		'BE' => 'Belgium',
		'BZ' => 'Belize',
		'BJ' => 'Benin',
		'BM' => 'Bermuda',
		'BT' => 'Bhutan',
		'BO' => 'Bolivia',
		'BA' => 'Bosnia And Herzegovina',
		'BW' => 'Botswana',
		'BV' => 'Bouvet Island',
		'BR' => 'Brazil',
		'IO' => 'British Indian Ocean Territory',
		'BN' => 'Brunei Darussalam',
		'BG' => 'Bulgaria',
		'BF' => 'Burkina Faso',
		'BI' => 'Burundi',
		'KH' => 'Cambodia',
		'CM' => 'Cameroon',
		'CA' => 'Canada',
		'CV' => 'Cape Verde',
		'KY' => 'Cayman Islands',
		'CF' => 'Central African Republic',
		'TD' => 'Chad',
		'CL' => 'Chile',
		'CN' => 'China',
		'CX' => 'Christmas Island',
		'CC' => 'Cocos (Keeling) Islands',
		'CO' => 'Colombia',
		'KM' => 'Comoros',
		'CG' => 'Congo',
		'CD' => 'Congo, Democratic Republic',
		'CK' => 'Cook Islands',
		'CR' => 'Costa Rica',
		'CI' => 'Cote D\'Ivoire',
		'HR' => 'Croatia',
		'CU' => 'Cuba',
		'CY' => 'Cyprus',
		'CZ' => 'Czech Republic',
		'DK' => 'Denmark',
		'DJ' => 'Djibouti',
		'DM' => 'Dominica',
		'DO' => 'Dominican Republic',
		'EC' => 'Ecuador',
		'EG' => 'Egypt',
		'SV' => 'El Salvador',
		'GQ' => 'Equatorial Guinea',
		'ER' => 'Eritrea',
		'EE' => 'Estonia',
		'ET' => 'Ethiopia',
		'FK' => 'Falkland Islands (Malvinas)',
		'FO' => 'Faroe Islands',
		'FJ' => 'Fiji',
		'FI' => 'Finland',
		'FR' => 'France',
		'GF' => 'French Guiana',
		'PF' => 'French Polynesia',
		'TF' => 'French Southern Territories',
		'GA' => 'Gabon',
		'GM' => 'Gambia',
		'GE' => 'Georgia',
		'DE' => 'Germany',
		'GH' => 'Ghana',
		'GI' => 'Gibraltar',
		'GR' => 'Greece',
		'GL' => 'Greenland',
		'GD' => 'Grenada',
		'GP' => 'Guadeloupe',
		'GU' => 'Guam',
		'GT' => 'Guatemala',
		'GG' => 'Guernsey',
		'GN' => 'Guinea',
		'GW' => 'Guinea-Bissau',
		'GY' => 'Guyana',
		'HT' => 'Haiti',
		'HM' => 'Heard Island & Mcdonald Islands',
		'VA' => 'Holy See (Vatican City State)',
		'HN' => 'Honduras',
		'HK' => 'Hong Kong',
		'HU' => 'Hungary',
		'IS' => 'Iceland',
		'IN' => 'India',
		'ID' => 'Indonesia',
		'IR' => 'Iran, Islamic Republic Of',
		'IQ' => 'Iraq',
		'IE' => 'Ireland',
		'IM' => 'Isle Of Man',
		'IL' => 'Israel',
		'IT' => 'Italy',
		'JM' => 'Jamaica',
		'JP' => 'Japan',
		'JE' => 'Jersey',
		'JO' => 'Jordan',
		'KZ' => 'Kazakhstan',
		'KE' => 'Kenya',
		'KI' => 'Kiribati',
		'KR' => 'Korea',
		'KW' => 'Kuwait',
		'KG' => 'Kyrgyzstan',
		'LA' => 'Lao People\'s Democratic Republic',
		'LV' => 'Latvia',
		'LB' => 'Lebanon',
		'LS' => 'Lesotho',
		'LR' => 'Liberia',
		'LY' => 'Libyan Arab Jamahiriya',
		'LI' => 'Liechtenstein',
		'LT' => 'Lithuania',
		'LU' => 'Luxembourg',
		'MO' => 'Macao',
		'MK' => 'Macedonia',
		'MG' => 'Madagascar',
		'MW' => 'Malawi',
		'MY' => 'Malaysia',
		'MV' => 'Maldives',
		'ML' => 'Mali',
		'MT' => 'Malta',
		'MH' => 'Marshall Islands',
		'MQ' => 'Martinique',
		'MR' => 'Mauritania',
		'MU' => 'Mauritius',
		'YT' => 'Mayotte',
		'MX' => 'Mexico',
		'FM' => 'Micronesia, Federated States Of',
		'MD' => 'Moldova',
		'MC' => 'Monaco',
		'MN' => 'Mongolia',
		'ME' => 'Montenegro',
		'MS' => 'Montserrat',
		'MA' => 'Morocco',
		'MZ' => 'Mozambique',
		'MM' => 'Myanmar',
		'NA' => 'Namibia',
		'NR' => 'Nauru',
		'NP' => 'Nepal',
		'NL' => 'Netherlands',
		'AN' => 'Netherlands Antilles',
		'NC' => 'New Caledonia',
		'NZ' => 'New Zealand',
		'NI' => 'Nicaragua',
		'NE' => 'Niger',
		'NG' => 'Nigeria',
		'NU' => 'Niue',
		'NF' => 'Norfolk Island',
		'MP' => 'Northern Mariana Islands',
		'NO' => 'Norway',
		'OM' => 'Oman',
		'PK' => 'Pakistan',
		'PW' => 'Palau',
		'PS' => 'Palestinian Territory, Occupied',
		'PA' => 'Panama',
		'PG' => 'Papua New Guinea',
		'PY' => 'Paraguay',
		'PE' => 'Peru',
		'PH' => 'Philippines',
		'PN' => 'Pitcairn',
		'PL' => 'Poland',
		'PT' => 'Portugal',
		'PR' => 'Puerto Rico',
		'QA' => 'Qatar',
		'RE' => 'Reunion',
		'RO' => 'Romania',
		'RU' => 'Russian Federation',
		'RW' => 'Rwanda',
		'BL' => 'Saint Barthelemy',
		'SH' => 'Saint Helena',
		'KN' => 'Saint Kitts And Nevis',
		'LC' => 'Saint Lucia',
		'MF' => 'Saint Martin',
		'PM' => 'Saint Pierre And Miquelon',
		'VC' => 'Saint Vincent And Grenadines',
		'WS' => 'Samoa',
		'SM' => 'San Marino',
		'ST' => 'Sao Tome And Principe',
		'SA' => 'Saudi Arabia',
		'SN' => 'Senegal',
		'RS' => 'Serbia',
		'SC' => 'Seychelles',
		'SL' => 'Sierra Leone',
		'SG' => 'Singapore',
		'SK' => 'Slovakia',
		'SI' => 'Slovenia',
		'SB' => 'Solomon Islands',
		'SO' => 'Somalia',
		'ZA' => 'South Africa',
		'GS' => 'South Georgia And Sandwich Isl.',
		'ES' => 'Spain',
		'LK' => 'Sri Lanka',
		'SD' => 'Sudan',
		'SR' => 'Suriname',
		'SJ' => 'Svalbard And Jan Mayen',
		'SZ' => 'Swaziland',
		'SE' => 'Sweden',
		'CH' => 'Switzerland',
		'SY' => 'Syrian Arab Republic',
		'TW' => 'Taiwan',
		'TJ' => 'Tajikistan',
		'TZ' => 'Tanzania',
		'TH' => 'Thailand',
		'TL' => 'Timor-Leste',
		'TG' => 'Togo',
		'TK' => 'Tokelau',
		'TO' => 'Tonga',
		'TT' => 'Trinidad And Tobago',
		'TN' => 'Tunisia',
		'TR' => 'Turkey',
		'TM' => 'Turkmenistan',
		'TC' => 'Turks And Caicos Islands',
		'TV' => 'Tuvalu',
		'UG' => 'Uganda',
		'UA' => 'Ukraine',
		'AE' => 'United Arab Emirates',
		'GB' => 'United Kingdom',
		'US' => 'United States',
		'UM' => 'United States Outlying Islands',
		'UY' => 'Uruguay',
		'UZ' => 'Uzbekistan',
		'VU' => 'Vanuatu',
		'VE' => 'Venezuela',
		'VN' => 'VietNam',
		'VG' => 'Virgin Islands, British',
		'VI' => 'Virgin Islands, U.S.',
		'WF' => 'Wallis And Futuna',
		'EH' => 'Western Sahara',
		'YE' => 'Yemen',
		'ZM' => 'Zambia',
		'ZW' => 'Zimbabwe'
		);
}

/**
 * make well documented and use function exists
 * @return array
 */

//all sex

function sex()
{
	return array(
		'male' => 'Male' ,
		'female' => 'Female' 
	);
}


/**
 * make well documented and use function exists
 * @return array
 */

//all activity

function activity()
{
	return array(
		'All' => 'all',
		'Morning' => '(6am to 12pm)',
		'Afternoon'=> '(12pm to 6pm)',
		'Evening' => '(6pm to 12am)',
		'Night' => '(12am to 6am)'
	);
}

function category()
{
    return array(
        'humour' => 'Humour & Memes',
        'fashion' => 'Fashoin & Style',
        'fitness' =>'Fitness & Sports',
        'quotes' => 'Quotes & Texts',
        'luxury' => 'Luxury & Motivation',
        'cars' => 'Cars & Bikes',
        'outdoor'=>'Outdoor & Travel',
        'food' => 'Food & Nutrition',
        'pets'=> 'Pet & Animal',
        'model' => 'Model & Lifestyle',
        'personal' => 'Personal & Talent',
        'music' => 'Music & Singers'
    );
}

//if(! function_exists('prevent_user_admin_page')) {
//	function prevent_user_admin_page () {
//		$redirect = isset( $_SERVER['HTTP_REFERER'] ) ? $_SERVER['HTTP_REFERER'] : home_url( '/' );
//		if (!current_user_can('manage_options')) {
//			exit( wp_redirect( $redirect ) );
//		}
//	}
//}


//add_action('admin_init', 'prevent_user_admin_page', 100);


if(! function_exists('fav_or_not')) {
	function fav_or_not($inf_id) {
		$getId = get_current_user_id();

		$getFav = wpFluent()->table('gphoebe_favorities')->where('user_id', $getId)->where('fav_id', $inf_id)->first();

		if($getFav) {
			return true;
		}

		return false;
	}
}


if(! function_exists('storeFavorite')){
	function storeFavorite(){

		$userId = get_current_user_id();

		$favArgs = array(
			'user_id' => $userId,
			'fav_id'  => $_REQUEST['fav']
		);

		$checkFavId = wpFluent()->table('gphoebe_favorities')->where('user_id', $userId)
															 ->where('fav_id',$_REQUEST['fav'])
															 ->first();

		if(! $checkFavId)
		{
			wpFluent()->table('gphoebe_favorities')->insert($favArgs);
		}
		else{
			wpFluent()->table('gphoebe_favorities')->where('user_id',$userId)->delete();
		}
	}
}

if(! function_exists('get_favorites')){
	function get_favorites(){
		$userId = get_current_user_id();

		$favUsers = wpFluent()->table('gphoebe_favorities')->where('user_id',$userId)
														   ->get();

		$arr = array();

		foreach ($favUsers as $key => $value) {
			$getF = wpFluent()->table('gphoebe_users')->where('user_id', $value->fav_id)->first();

			array_push($arr, $getF);
		}

		return $arr;
		
	}
}

if(! function_exists('storeUserData')){
	function storeUserData(){
		$user_id = get_current_user_id();
		
		$user_name = $_REQUEST['user_name'];
		
		$phone_number = sanitize_text_field( $_REQUEST['phone_number'] );
		
		$register_date = sanitize_text_field( $_REQUEST['register_date'] );
		
		$languages = sanitize_text_field( $_REQUEST['languages'] );
		
		$countries = sanitize_text_field( $_REQUEST['countries'] );
		
		$age = intval( $_REQUEST['age'] );
		
		$sex = sanitize_text_field( $_REQUEST['sex'] );
		
		$activity = sanitize_text_field( $_REQUEST['activity'] );

		$category = sanitize_text_field( $_REQUEST['category'] );
		
		$description = sanitize_textarea_field($_REQUEST['description'] );

		$arg = array(
				'user_id' => $user_id,
				'user_name'=> $user_name,
				'phone_number' => $phone_number,
				'registration_date' => $register_date,
				'language' => $languages,
				'country' => $countries,
				'age' => $age,
				'sex' => $sex,
				'activity' => $activity,
				'description' => $description
		);

		$get_data= wpFluent()->table('gphoebe_users')->where('user_id', $user_id )->first();

		$get_categorie_data= wpFluent()->table('ghoebe_user_category')->where('user_id', $user_id )->first();

		$arg2 = array(
				'phone_number' => $phone_number,
				'language' => $languages,
				'country' => $countries,
				'age' => $age,
				'sex' => $sex,
				'activity' => $activity,
				'description' => $description
		);

		$category_data = array(
						'user_id' => $user_id,
						'category_id' => $category

		);

		if($get_categorie_data){
			wpFluent()->table('gphoebe_user_category')->where('user_id',$user_id)->update($category_data);
		}

		else{

			wpFluent()->table('gphoebe_user_category')->insert($category_data);
		}

		if($get_data){
			$user_id = get_current_user_id();

			wpFluent()->table('gphoebe_users')->where('user_id', $user_id )->update($arg2);
		}
		else{

			wpFluent()->table('gphoebe_users')->insert($arg);
		}
	}
}

if(! function_exists('getUserDetails')){
	function getUserDetails(){

		$userId = get_current_user_id();

		$getUserData = wpFluent()->table('gphoebe_users')->where('user_id',$userId)->first();

		return $getUserData;
	}
} 


if(! function_exists('convert_follower')) {
	function convert_follower($inf) {
		$getF = wpFluent()->table('gphoebe_user_social_account')->where('user_id', $inf)->first()->followers;
		$num = $getF / 1000;
		return $newVal = number_format($num,2) . 'k';
	}
}

if(! function_exists('inf_score')) {
	function inf_score($inf) {
		$infTotal = wpFluent()->table('gphoebe_order_details')->where('influencer_id', $inf)->whereNot('order_status', 'pending')->count();

		$infCompleted = wpFluent()->table('gphoebe_order_details')->where('influencer_id', $inf)->where('order_status', 'completed')->count();

		if($infCompleted == 0 || $infTotal == 0) {
			return 0;
		}

		return (($infCompleted) / ($infTotal)*10);
	}
}

if (! function_exists('inf_price')) {
	function inf_price($inf) {
		$infPrice = wpFluent()->table('gphoebe_pricing')->where('user_id', $inf)->where('display', 1)->first();

		return $infPrice ? $infPrice->price : 0.00;
	}
}

if(! function_exists('get_all_categories')) {
	function get_all_categories() {
		$getAllCategories = wpFluent()->table('gphoebe_category')->get();

		return $getAllCategories;
	}
}

if(! function_exists('not_admin')) {
	function not_admin() {
		if(is_admin() || current_user_can('manage_options')) {
			wp_redirect(home_url('/wp-admin'));
		}
	}
}


if(! function_exists('verify_instagram')) {
	function verify_instagram() {


		if(true) {
			$userName = sanitize_text_field( $_REQUEST['username'] );

			if( $userName ) {
				$url = "https://www.instagram.com/" . $userName . '/?__a=1';
				$details = json_decode(file_get_contents($url));

				if ($details->graphql->user->edge_followed_by->count >= get_option('gphoebe_settings')['followers_number'] )  {

					$args = array(
						'user_id' => get_current_user_id(),
						'user_name' => $userName,
						'account_type' => 'instagram',
						'social_user_id' => $details->graphql->user->id,
						'followers' => $details->graphql->user->edge_followed_by->count,
						'dp'		=> $details->graphql->user->profile_pic_url
					);

					wpFluent()->table('gphoebe_user_social_account')->insert($args);

					$argsUser = array(
						'is_buyer' => '0'
					);

					wpFluent()->table('gphoebe_users')->where('user_id', get_current_user_id())->update($argsUser);

					return 200;
				} else {
					return 400;
				}
			} else {
				return 301;
			}
		} else {
			return 500;
		}

		
	}
}

if(! function_exists('my_social_account')) {
	function my_social_account() {
		return wpFluent()->table('gphoebe_user_social_account')->where('user_id', get_current_user_id())->get();
	}
}

if(! function_exists('remove_account')) {
	function remove_account() {
		$getUserId = get_current_user_id();
		$getAccountType = sanitize_text_field( $_REQUEST['atype'] );

		$getSocialUserId = sanitize_text_field( $_REQUEST['social_user_id'] );

		wpFluent()->table('gphoebe_user_social_account')
		           ->where('user_id', $getUserId)
		           ->where('account_type', $getAccountType)
		           ->where('social_user_id', $getSocialUserId)
		           ->delete();


		wpFluent()->table('gphoebe_users')->where('user_id', get_current_user_id())->update($argsUser);

		return true;
	}
}