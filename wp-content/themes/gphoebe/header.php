<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package gphoebe
 */
session_start();
?>
<!doctype html>
<html <?php language_attributes(); ?>>

<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
    <script src="https://www.paypalobjects.com/api/checkout.js" data-version-4></script>
<!--    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">-->

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">

    <?php if(! is_page('my-account')) : ?>
    <?php if(! is_page('first-home')) : ?>
    <?php if(! is_page('influencer-section')) : ?>
    <?php if(! is_page('business-section')) : ?>
    <?php if(! is_page('user-profile')) : ?>
    <?php if(! is_page('bank')) : ?>
    <?php if(! is_page('create-package')) : ?>


	<header id="masthead" class="site-header">

        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="fa fa-bars"></span>
                </button>
                <a class="navbar-brand" href="<?php _e(home_url()); ?>">
                    <img width="140" src="assets/images/logo.png" alt="<?php echo get_bloginfo('name');?>" title="<?php echo get_bloginfo('name');?>"/></a>
            </div>



            <div class="collapse menu navbar-collapse" id="collapse">
                
                    <?php
                        global $user_ID;

                        if(! is_user_logged_in()) {
                            wp_nav_menu( array(
                                'menu_class'    => 'nav navbar-nav navbar-right',
                                'theme_location' => 'menu-1',
                                'container'     => 'ul'
                            ) );
                        } else {
	                        wp_nav_menu( array(
		                        'menu_class'    => 'nav navbar-nav navbar-right',
		                        'theme_location' => 'menu-2',
		                        'container'     => 'ul'
	                        ) );
                        }
                        
                    ?>

            </div>
        </div>
	</header><!-- #masthead -->
    <?php endif; ?>
    <?php endif; ?>
    <?php endif; ?>
    <?php endif; ?>
    <?php endif; ?>
    <?php endif; ?>
    <?php endif; ?>
    
	<div id="content" class="site-content">
