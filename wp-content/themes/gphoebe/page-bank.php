<?php
/**
 * Template Name: Bank Page
 *
 * @package gphoebe
 */

/**
 * Check user logged in
 */
not_admin();

check_user_logged_in();

get_header();
$successWithdrawPaypal = false;

if ($_SERVER['REQUEST_METHOD'] == "POST") {
    if (array_key_exists('request_method', $_REQUEST)) {
        if ($_REQUEST['request_method'] == 'paypal') {
	        $successWithdrawPaypal = send_withdraw_paypal_request();
        }
    }
}

?>
    <script src="https://checkout.stripe.com/checkout.js"></script>

<div class="wrapper">
	<?php
		gphoebe_sidebar();
	?>
	<div class="main-panel">
		<nav class="navbar navbar-transparent navbar-absolute">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="#"> Bank </a>
                    </div>

                </div>
        </nav>
        <div class="content">
        	<div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12 col-md-12">
                        <div class="card card-nav-tabs">
                            <div class="card-header" data-background-color="red">
                                <div class="nav-tabs-navigation">
                                    <div class="nav-tabs-wrapper">
                                        <!-- <span class="nav-tabs-title">Tasks:</span> -->
                                        <ul class="nav nav-tabs" data-tabs="tabs">
                                            <li class="<?php bank_tab_checking('add_fund'); ?>">
                                                <a href="<?php bank_route('add_fund'); ?>" data-toggle="tab">
                                                    Add Funds
                                                    <div class="ripple-container"></div>
                                                </a>
                                            </li>
                                            <li class="<?php bank_tab_checking('withdraw'); ?>">
                                                <a class="<?php _e(withdraw_front_check()); ?>"  href="<?php bank_route('withdraw'); ?>" data-toggle="tab">
                                                    Withdraw
                                                    <div class="ripple-container"></div>
                                                </a>
                                            </li>
                                            <li class="<?php bank_tab_checking('transaction'); ?>">
                                                <a  href="<?php bank_route('transaction'); ?>" data-toggle="tab">
                                                    Transaction
                                                    <div class="ripple-container"></div>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>


                            </div>
                            <?php if (bank_tab_content('add_fund')): ?>
                                <div class="card-content">
                                    <div class="tab-content">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group label-floating">
                                                    <label class="control-label">Amount</label>
                                                    <input placeholder="Add your fund amount" type="text" class="form-control fund-amount">
                                                </div>
                                            </div>
                                        </div>
                                        <div id="myContainerElement"></div>
                                    </div>
                                </div>
                            <?php elseif (bank_tab_content('withdraw')): ?>
                                 <div class="card-content">
                                    <div class="tab-content">
                                        <?php if($successWithdrawPaypal): ?>
                                        <div class="alert alert-success">
                                            <strong>Successfully Send Payment Withdraw Request !</strong>.
                                        </div>
                                        <?php endif; ?>
                                        <form method="post">
                                            <input type="hidden" name="request_method" value="paypal">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group label-floating">
                                                        <label class="control-label">Paypal Email</label>
                                                        <input placeholder="Enter Paypal Email" name="request_email" type="email" class="form-control">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group label-floating">
                                                        <label class="control-label">Amount</label>
                                                        <input placeholder="Add your fund amount" name="request_amount" type="text" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <button class="btn btn-primary">Withdraw Request</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            <?php elseif (bank_tab_content('transaction')): ?>
                                <div class="card-content">
                                    <div class="tab-content">
                                        <table class="table">
                                            <tbody>
                                                <?php foreach (get_all_transactions() as $trans): ?>
                                                    <tr>
                                                        <td><?php _e($trans); ?></td>
                                                    </tr>
                                                <?php endforeach; ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
	</div>

</div>

        <script>
            // var a = {};
            // a = StripeCheckout.configure({
            //     key: 'pk_test_BhCTiW2pGG0CfREq0sGuBdBJ',
            //     image: 'https://stripe.com/img/documentation/checkout/marketplace.png',
            //     locale: 'auto',
            //     token: (token) =>  {
            //         console.log(token)
            //     }
            // });
            // a.open({
            //     name: 'Demo Site',
            //     description: '2 widgets',
            //     amount: 2000
            // });

        </script>

        <script>
            // Render the button into the container element


        </script>




<?php

get_footer();
