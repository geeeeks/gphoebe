<?php
/**
 * Template Name: Browser Page
 *
 * @package gphoebe
 */

get_header();

browse();

?>

<div class="main-panel">
        <nav class="navbar navbar-transparent navbar-absolute">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="#"> Influencer Section </a>
                    </div>
                </div>
        </nav>
</div>

<div class="container">
        <div class="content">
            <div class="page-header">
                <h1 class="hidden-xs">
                                            Buy shoutouts from social media Influencers<br/>
                        <small>Browse social media influencers by category, followers and price</small>
                                    </h1>
                <h3 class="visible-xs">
                                            Buy shoutouts from social media Influencers<br/>
                        <small>Browse social media influencers by category, followers and price</small>
                                    </h3>
            </div>
                <div class="row">
                    <form method="GET" action="<?php echo $_SERVER['REQUEST_URI']; ?>" id="custom-search-input" class="col-md-12">
                        <div class="input-group col-md-12">
                            <input type="hidden" name="c_p" value="1">
                            <input name="search" type="text" id="search_keywords" class="search-query form-control"
                                   placeholder="search influencers by keyword, e.g. video, nail, hair">
                    <span class="input-group-btn">
                        <button class="btn btn-success" type="button" id="button_search">
                            <span class="glyphicon glyphicon-search"></span>
                        </button>
                    </span>
                        </div>
                        <div>&nbsp;</div>
                    </form>
                </div>
            <div class="row">
                    <div class="col-md-3">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <a href="#" class="navbar-toggle collapsed" data-toggle="collapse"
                                   data-target="#collapse-row"
                                   style="float:none;margin:0px;padding:0px;color:#666666;"><span
                                            class="fa fa-bars"></span></a>
                                <strong>Refine your search</strong>
                            </div>
                        </div>


                        <div id="collapse-row" class="collapse navbar-collapse" style="padding:0px;">

                                                             
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <span class="panel-title">Categories</span>
                                    </div>
                                    <!-- input-group -->
                                    <ul id="check-list-box" class="list-group">
                                                                                                                        <li class="list-group-item"><label>
                                                <input type="checkbox" id="checkAll" checked aria-label="All"> All</label>
                                            <a href="<?php _e(home_url('/browser/?c_p=1')); ?>"><i class="fa fa-sign-out"
                                                                        aria-hidden="true"></i></a>
                                        </li>
                                        
                                                                                                                                    <li class="list-group-item"><label>
                                                    <input type="checkbox" value="1" class="checkCategory" checked aria-label="Humour &amp; Memes">  Humour &amp; Memes
                                                    <a href="<?php _e(home_url('/browser/?c=1&c_p=1')); ?>"><i class="fa fa-sign-out"
                                                                                aria-hidden="true"></i></a>
                                                </label>
                                            </li>
                                        
                                                                                                                                    <li class="list-group-item"><label>
                                                    <input type="checkbox" value="2" class="checkCategory" checked aria-label="Fashion &amp; Style">  Fashion &amp; Style
                                                    <a href="<?php _e(home_url('/browser/?c=2&c_p=1')); ?>""><i class="fa fa-sign-out"
                                                                                aria-hidden="true"></i></a>
                                                </label>
                                            </li>
                                        
                                                                                                                                    <li class="list-group-item"><label>
                                                    <input type="checkbox" value="3" class="checkCategory" checked aria-label="Fitness &amp; Sports">  Fitness &amp; Sports
                                                    <a href="<?php _e(home_url('/browser/?c=3&c_p=1')); ?>"><i class="fa fa-sign-out"
                                                                                aria-hidden="true"></i></a>
                                                </label>
                                            </li>
                                        
                                                                                                                                    <li class="list-group-item"><label>
                                                    <input type="checkbox" value="4" class="checkCategory" checked aria-label="Quotes &amp; Texts">  Quotes &amp; Texts
                                                    <a href="<?php _e(home_url('/browser/?c=4&c_p=1')); ?>"><i class="fa fa-sign-out"
                                                                                aria-hidden="true"></i></a>
                                                </label>
                                            </li>
                                        
                                                                                                                                    <li class="list-group-item"><label>
                                                    <input type="checkbox" value="5" class="checkCategory" checked aria-label="Luxury &amp; Motivation">  Luxury &amp; Motivation
                                                    <a href="<?php _e(home_url('/browser/?c=5&c_p=1')); ?>"><i class="fa fa-sign-out"
                                                                                aria-hidden="true"></i></a>
                                                </label>
                                            </li>
                                        
                                                                                                                                    <li class="list-group-item"><label>
                                                    <input type="checkbox" value="6" class="checkCategory" checked aria-label="Cars &amp; Bikes">  Cars &amp; Bikes
                                                    <a href="<?php _e(home_url('/browser/?c=6&c_p=1')); ?>"><i class="fa fa-sign-out"
                                                                                aria-hidden="true"></i></a>
                                                </label>
                                            </li>
                                        
                                                                                                                                    <li class="list-group-item"><label>
                                                    <input type="checkbox" value="7" class="checkCategory" checked aria-label="Outdoor &amp; Travel">  Outdoor &amp; Travel
                                                    <a href="<?php _e(home_url('/browser/?c=7&c_p=1')); ?>"><i class="fa fa-sign-out"
                                                                                aria-hidden="true"></i></a>
                                                </label>
                                            </li>
                                        
                                                                                                                                    <li class="list-group-item"><label>
                                                    <input type="checkbox" value="8" class="checkCategory" checked aria-label="Food &amp; Nutrition">  Food &amp; Nutrition
                                                    <a href="<?php _e(home_url('/browser/?c=8&c_p=1')); ?>"><i class="fa fa-sign-out"
                                                                                aria-hidden="true"></i></a>
                                                </label>
                                            </li>
                                        
                                                                                                                                    <li class="list-group-item"><label>
                                                    <input type="checkbox" value="9" class="checkCategory" checked aria-label="Pets &amp; Animals">  Pets &amp; Animals
                                                    <a href="<?php _e(home_url('/browser/?c=9&c_p=1')); ?>"><i class="fa fa-sign-out"
                                                                                aria-hidden="true"></i></a>
                                                </label>
                                            </li>
                                        
                                                                                                                                    <li class="list-group-item"><label>
                                                    <input type="checkbox" value="10" class="checkCategory" checked aria-label="Models &amp; Lifestyle">  Models &amp; Lifestyle
                                                    <a href="<?php _e(home_url('/browser/?c=10&c_p=1')); ?>"><i class="fa fa-sign-out"
                                                                                aria-hidden="true"></i></a>
                                                </label>
                                            </li>
                                        
                                                                                                                                    <li class="list-group-item"><label>
                                                    <input type="checkbox" value="11" class="checkCategory" checked aria-label="Personal &amp; Talent">  Personal &amp; Talent
                                                    <a href="<?php _e(home_url('/browser/?c=11&c_p=1')); ?>"><i class="fa fa-sign-out"
                                                                                aria-hidden="true"></i></a>
                                                </label>
                                            </li>
                                                                            </ul>
                                    <!-- /input-group -->
                                </div>
                                
                                                                                                <div class="panel panel-default">
                                    <!-- <div class="panel-heading">
                                        <span class="panel-title">Demographics</span> <span class="label label-success">new</span>
                                    </div> -->
                                   <!--  <div class="panel-body">
                                        <p><span><i class="fa fa-lock"></i></span> <small>Filter follower audiences by Language, Country, Sex or Age. Available for registered members only. <a href="auth/login.html">Login</a> to access.</small></p>
                                    </div> -->
                                </div>
                                                            <!-- <div class="panel panel-default">
                                <div class="panel-heading">
                                    <span class="panel-title">Followers</span>
                                </div>
                                <div class="panel-body">
                                    <div class="range-markers">
                                        <span id="followLower" class="lower ng-binding">0</span>
                                        <span id="followUpper" class="upper ng-binding">5 m</span>
                                    </div>
                                    <div id="followsSlide"></div>
                                    <button id="changeFollowers" class="btn margin-top-15 pull-right btn-success">Update
                                    </button>
                                </div>
                            </div> -->
                           <!--  <div class="panel panel-default">
                                <div class="panel-heading">
                                    <span class="panel-title">Score</span>
                                </div>
                                <div class="panel-body">
                                    <div class="range-markers">
                                        <span id="scoreLower" class="lower ng-binding">0</span>
                                        <span id="scoreUpper" class="upper ng-binding">30</span>
                                    </div>
                                    <div id="scoreSlide"></div>
                                    <button id="changeScore" class="btn margin-top-15 pull-right  btn-success">Update
                                    </button>


                                </div>
                            </div> -->
                        </div>

                    </div>
                <div class="col-md-9">
                    <div class="panel panel-default">

                            <!-- <div class="panel-body" style="padding: 15px 15px 0 15px;">
                                <div class="pull-left">
                                    <div id="tags"></div>
                                </div>
                                <div class="dropdown pull-right">
                                    <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenu1"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">Sort
                                        By <span
                                                class="caret"></span></button>
                                    <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                        <li class="order" data-name="price"><a href="#">Price
                                                                                            </a></li>
                                        <li class="order" data-name="followers"><a href="#">Followers
                                                                                            </a></li>
                                        <li class="order" data-name="score"><a href="#">Score
                                                                                            </a></li>
                                    </ul>
                                </div>
                            </div> -->
                        <div class="panel-body">

                            <table class="table">
                                <thead>
                                <th>Account</th>
                                <th>Followers
                                    <small class="text-muted visible-xs visible-sm">Score</small>
                                </th>
                                <th class="hidden-sm hidden-xs">Score <a data-toggle="tooltip" data-placement="left"
                                                                         title="Shoutcart Score is a proprietary formula that determines authenticity of engagement and followers of the influencer, including their total reach. It is updated on a daily basis, and is unique to each influencer. Higher score = higher reach and engagement!"><i
                                                class="fa fa-question hidden-xs"></i></a></th>
                                <th>Price</th>
                                </thead>
                                <?php foreach (browse()['data'] as $key => $inf): ?>
                                <tr>
                                        <td>
                                            <img src="<?php _e($inf->dp); ?>" class="cart-image img-circle">
                                            <a href="<?php top18UserUrl($inf->user_name); ?>"><span>@</span><?php _e($inf->user_name); ?>
                                            </a>
                                        </td>
                                        <td><?php _e(convert_follower($inf->user_id));?>
                                            <small class="text-muted visible-xs visible-sm">7.43</small>
                                        </td>
                                        <td class="hidden-sm hidden-xs"><?php _e(inf_score($inf->user_id)); ?></td>
                                        <td>
                                            <?php if(!is_user_logged_in()): ?>
                                            <a data-toggle="tooltip" data-placement="left"
                                               title="Login or register to view pricing">
                                                <i class="fa fa-lock"></i></a>
                                            <?php else: ?>
                                                <?php _e(inf_price($inf->user_id)); ?>
                                            <?php endif; ?>
                                        </td>
                                </tr>
                                <?php endforeach; ?>
                            </table>
                            <input type="hidden" id="age"
                                   value="">
                            <input type="hidden" id="sex"
                                   value="">
                            <input type="hidden" id="activity"
                                   value="">
                            <input type="hidden" id="followers"
                                   value="">
                            <input type="hidden" id="language"
                                   value="">
                            <input type="hidden" id="country"
                                   value="">
                            <input type="hidden" id="social"
                                   value="">
                            <input type="hidden" id="score"
                                   value="">
                            <input type="hidden" id="cat"
                                   value="">
                            <input type="hidden" id="keywords"
                                   value="">
                            <input type="hidden" id="sort"
                                   value="">
                            <input type="hidden" id="attr"
                                   value="">
                        </div>
                        <div class="panel-footer text-center">
                            <nav>
                                <ul class="pagination">

                                    <li class="disabled"><span>&laquo;</span></li>
                                    <?php foreach (range(1, browse()['last_page']) as $value): ?>
                                       
                                        <?php if (browse()['current_page'] == $value): ?>
                                            <li class="active"><span><?php _e($value); ?></span></li>
                                        <?php else: ?>

                                            <li><a href="<?php browse_paginate($value); ?>"><span><?php _e($value); ?></span></a></li>
	                                    <?php endif; ?>
                                    <?php endforeach; ?>
                        </div>

                    </div>
                </div>
            </div>
        </div>
</div>


<?php

get_footer();