<?php
/**
 * Template Name: Business Section Page
 *
 * @package gphoebe
 */
not_admin();

get_header();

$successShortCode = false;

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    if (array_key_exists('order_details_id', $_REQUEST)) {
	    inf_order_status(
	            $_REQUEST['order_details_id'],
                $_REQUEST['influencer_id'],
                $_REQUEST['status']
        );
    }
    else if(array_key_exists('add_short_code', $_REQUEST)) {
        $successShortCode = add_details_shortcode();
    }
}

?>

<div class="wrapper">
    <?php
        gphoebe_sidebar();
    ?>
    <div class="main-panel">
        <nav class="navbar navbar-transparent navbar-absolute">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="#"> Business Section </a>
                    </div>
                </div>
        </nav>
        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12 col-md-12">
                        <div class="card card-nav-tabs">
                            <div class="card-header" data-background-color="red">
                                <div class="nav-tabs-navigation">
                                    <div class="nav-tabs-wrapper">
                                        <!-- <span class="nav-tabs-title">Tasks:</span> -->
                                        <ul class="nav nav-tabs" data-tabs="tabs">

                                            <?php if( ! my_details()->is_buyer): ?>
                                                <li class="<?php business_section_route_active('all_selling_orders'); ?>">
                                                    <a href="<?php business_section_route_gen('all_selling_orders'); ?>" data-toggle="tab">
                                                        All Selling Orders
                                                        <div class="ripple-container"></div>
                                                    </a>
                                                </li>
                                            <?php endif; ?>

                                            <li class="<?php business_section_route_active('all_buying_orders'); ?>">
                                                <a href="<?php business_section_route_gen('all_buying_orders'); ?>" data-toggle="tab">
                                                    All Buying Orders
                                                    <div class="ripple-container"></div>
                                                </a>
                                            </li>

                                             <li class="<?php business_section_route_active('my_media'); ?>">
                                                <a href="<?php business_section_route_gen('my_media'); ?>" data-toggle="tab">
                                                    My Media
                                                    <div class="ripple-container"></div>
                                                </a>
                                            </li>

                                        </ul>
                                    </div>
                                </div>


                            </div>
                            <?php if(business_section_tab_content('all_earnings')): ?>
                                <div class="card-content">
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="profile">
                                            <table class="table">
                                                <thead>
                                                <tr>
                                                    <th>Firstname</th>
                                                    <th>Lastname</th>
                                                    <th>Email</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                     <tr>
                                                        <td>Sign contract for "What are conference organizers afraid of?"</td>
                                                    </tr> 
                                                    <tr>
                                                        <td>Lines From Great Russian Literature? Or E-mails From My Boss?</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Flooded: One year later, assessing what was lost and what was found when a ravaging rain swept through metro Detroit
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Create 4 Invisible User Experiences you Never Knew About</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        
                                    </div>
                                </div>
                            <?php elseif(business_section_tab_content('all_selling_orders')): ?>
                                <div class="card-content">
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="profile">
                                            <table class="table">
                                                <thead>
                                                <tr>
                                                    <th>Description</th>
                                                    <th>Buyer</th>
                                                    <th>Status</th>
                                                    <th>Payment</th>
                                                    <th>Action</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                    <?php foreach (my_all_orders()['all_orders'] as $value): ?>
                                                     <tr>
                                                        <td><?php _e($value->order_description); ?></td>
                                                         <td><?php _e(buyer_details($value->buyer_id)->user_name); ?></td>
                                                         <td><span class="<?php _e(make_status_class($value->order_status)); ?>" style="padding: 8px 10px; text-transform: capitalize;"> <?php _e($value->order_status); ?> </span></td>
                                                         <td><span class="<?php _e(make_status_class($value->payment_status)); ?>" style="padding: 8px 10px; text-transform: capitalize;"> <?php _e($value->payment_status); ?> </span></td>
                                                         <td style="display: inline-flex;">
	                                                         <?php if ($value->order_status == 'invited' && ! my_details()->is_buyer): ?>
                                                                 <form method="POST" class="form-inline">
                                                                     <input type="hidden" name="order_details_id" value="<?php _e($value->id); ?>">
                                                                     <input type="hidden" name="influencer_id" value="<?php _e(get_current_user_id()); ?>">
                                                                     <input type="hidden" name="status" value="accept">
                                                                     <button class="btn btn-success" style="padding: 8px 10px;">Accept</button>
                                                                 </form>
                                                                 <form action="" method="POST" class="form-inline">
                                                                     <input type="hidden" name="order_details_id" value="<?php _e($value->id); ?>">
                                                                     <input type="hidden" name="influencer_id" value="<?php _e(get_current_user_id()); ?>">
                                                                     <input type="hidden" name="status" value="reject">
                                                                     <button class="btn btn-danger" style="padding: 8px 10px;">Reject</button>
                                                                 </form>
	                                                         <?php endif; ?>

	                                                         <?php if ($value->order_status == 'pending' && ! my_details()->is_buyer): ?>
                                                                 <form method="POST" class="form-inline">
                                                                     <input type="hidden" name="order_details_id" value="<?php _e($value->id); ?>">
                                                                     <input type="hidden" name="influencer_id" value="<?php _e(get_current_user_id()); ?>">
                                                                     <input type="hidden" name="status" value="delivered">
                                                                     <button class="btn btn-success" style="padding: 8px 10px;">Deliver</button>
                                                                 </form>
                                                                 <form method="POST" class="form-inline">
                                                                     <input type="hidden" name="order_details_id" value="<?php _e($value->id); ?>">
                                                                     <input type="hidden" name="influencer_id" value="<?php _e(get_current_user_id()); ?>">
                                                                     <input type="hidden" name="status" value="cancelled">
                                                                     <button class="btn btn-danger" style="padding: 8px 10px;">Cancel</button>
                                                                 </form>
	                                                         <?php endif; ?>
	                                                         <?php if ($value->order_status == 'revision' && ! my_details()->is_buyer): ?>
                                                                 <form method="POST" class="form-inline">
                                                                     <input type="hidden" name="order_details_id" value="<?php _e($value->id); ?>">
                                                                     <input type="hidden" name="influencer_id" value="<?php _e(get_current_user_id()); ?>">
                                                                     <input type="hidden" name="status" value="delivered">
                                                                     <button class="btn btn-success" style="padding: 8px 10px;">Re-Deliver</button>
                                                                 </form>
                                                                 <form method="POST" class="form-inline">
                                                                     <input type="hidden" name="order_details_id" value="<?php _e($value->id); ?>">
                                                                     <input type="hidden" name="influencer_id" value="<?php _e(get_current_user_id()); ?>">
                                                                     <input type="hidden" name="status" value="cancelled">
                                                                     <button class="btn btn-danger" style="padding: 8px 10px;">Cancel</button>
                                                                 </form>
	                                                         <?php endif; ?>

                                                             <a href="<?php _e(home_url('/business-section/?tab=details&inf='.buyer_details($value->influencer_id)->user_name.'&order='.$value->id.'&inf_id='. $value->influencer_id)); ?>" class="btn btn-info" style="padding: 8px 10px;">Details</a>
                                                         </td>
                                                    </tr>
                                                    <?php endforeach; ?>
                                                </tbody>
                                            </table>
                                        </div>
                                        
                                    </div>
                                </div>
                            <?php elseif(business_section_tab_content('all_buying_orders')) : ?>
                                <div class="card-content">
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="profile">
                                            <table class="table">
                                                <thead>
                                                <tr>
                                                    <th>Description</th>
                                                    <th>Influencer</th>
                                                    <th>Status</th>
                                                    <th>Payment</th>
                                                    <th>Action</th>
                                                </tr>
                                                </thead>
                                                <tbody>
					                            <?php foreach (my_buying_orders()['all_orders'] as $value): ?>
                                                    <tr>
                                                        <td><?php _e($value->order_description); ?></td>
                                                        <td><?php _e(buyer_details($value->influencer_id)->user_name); ?></td>
                                                        <td><span class="<?php _e(make_status_class($value->order_status)); ?>" style="padding: 8px 10px; text-transform: capitalize;"> <?php _e($value->order_status); ?> </span></td>
                                                        <td><span class="<?php _e(make_status_class($value->payment_status)); ?>" style="padding: 8px 10px; text-transform: capitalize;"> <?php _e($value->payment_status); ?> </span></td>
                                                        <td style="display: inline-flex;">
								                            <?php if ($value->order_status == 'delivered'): ?>
                                                                <form method="POST" class="form-inline">
                                                                    <input type="hidden" name="order_details_id" value="<?php _e($value->id); ?>">
                                                                    <input type="hidden" name="buyer_id" value="<?php _e(get_current_user_id()); ?>">
                                                                    <input type="hidden" name="influencer_id" value="<?php _e($value->influencer_id); ?>">
                                                                    <input type="hidden" name="status" value="completed">
                                                                    <button class="btn btn-success" style="padding: 8px 10px;">Completed</button>
                                                                </form>
                                                                <form action="" method="POST" class="form-inline">
                                                                    <input type="hidden" name="order_details_id" value="<?php _e($value->id); ?>">
                                                                    <input type="hidden" name="buyer_id" value="<?php _e(get_current_user_id()); ?>">
                                                                    <input type="hidden" name="influencer_id" value="<?php _e($value->influencer_id); ?>">
                                                                    <input type="hidden" name="status" value="revision">
                                                                    <button class="btn btn-warning" style="padding: 8px 10px;">Revision</button>
                                                                </form>
                                                            <?php endif; ?>

                                                            <a href="<?php _e(home_url('/business-section/?tab=details&inf='.buyer_details($value->influencer_id)->user_name.'&order='.$value->id.'&inf_id='. $value->influencer_id)); ?>" class="btn btn-info" style="padding: 8px 10px;">Details</a>
                                                        </td>
                                                    </tr>
					                            <?php endforeach; ?>
                                                </tbody>
                                            </table>
                                        </div>

                                    </div>
                                </div>
                            <?php elseif(business_section_tab_content('my_media')): ?>
                                 <div class="card-content">
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="profile">
                                            <table class="table">
                                                <thead>
                                                <tr>
                                                    <th>Media</th>
                                                    <th>Description</th>
                                                    <th>Shortcode</th>
                                                </tr>
                                                </thead>
                                                <tbody class="media-insta">
                                                     
                                                </tbody>
                                            </table>
                                        </div>
                                        
                                    </div>
                                </div>
                            <?php elseif(business_section_tab_content('details')): ?>
                                <div class="card-content">
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="profile">
                                            <div class="row details-show">
                                                
                                            </div>
                                            <?php if($successShortCode): ?>
                                            <div class="alert alert-success">
                                                <span>
                                                    <strong>
                                                        Successfully Added!
                                                    </strong>
                                                </span>
                                            </div>
                                            <?php endif; ?>

                                            <?php if(get_current_user_id() == $_REQUEST['inf_id']): ?>
                                                <form method="POST"> 
                                                    <input type="hidden" name="add_short_code">
                                                    <input type="hidden" name="inf_id" value="<?php _e($_REQUEST['inf_id']); ?>">
                                                    <input type="hidden" name="order_id" value="<?php _e($_REQUEST['order']); ?>">
                                                    <div class="col-md-6">
                                                        <div class="form-group label-floating">
                                                            <label class="control-label">Add Short Code</label>
                                                            <input type="text" name="short_code" class="form-control">
                                                        </div>
                                                        <button class="btn btn-primary">
                                                            Add Short Code
                                                        </button>
                                                    </div>  
                                                </form>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>



<?php

get_footer();