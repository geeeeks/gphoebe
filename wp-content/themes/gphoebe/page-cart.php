<?php
/**
 * Template Name: Cart Page
 *
 * @package gphoebe
 */
/**
 * Check user logged in
 */
not_admin();

check_user_logged_in();

get_header();

$showSuccessInvitation = false;

if ($_SERVER['REQUEST_METHOD'] == 'POST' ) {
    if (array_key_exists('key', $_REQUEST)) {
	    $key = intval($_REQUEST['key']);
	    cart_remove_by_key($key);
    }

    if (array_key_exists('order_submit', $_REQUEST)) {
	    $showSuccessInvitation = add_order_invitation();
    }

}

$cartData = cart_show();
$total = 0;

foreach ($cartData as $value) {
    $total += $value['price'];
}
?>

<div class="container">
    <div class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="panel panel-default" style="margin-top:1em;">
                    <div class="panel-heading">Cart Summary</div>
                    <div class="panel-body">
                        <table class="table">
                            <thead>
                            <tr>
                                <th>Username</th>
                                <th>Price</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($cartData as $key => $inf): ?>
                                <tr>
                                    <td>
                                        <img src="<?php _e($inf['dp']); ?>" class="cart-image img-circle">
                                        <span>@</span><?php _e($inf['user_name']); ?>  <i class="fa fa-instagram " aria-hidden="true"></i>
                                    </td>
                                    <td>
                                        <span>$</span><?php _e($inf['price']); ?>
                                    </td>
                                    <td>
                                        <form method="POST">
                                            <input type="hidden" name="key" value="<?php _e($inf['key']); ?>">
                                            <button class="btn btn-danger glyphicon glyphicon-remove" aria-hidden="true"></button>
                                        </form>
                                    </td>
                                </tr>
                                <?php endforeach; ?>
                            </tbody>
                            <?php if ($total > 0): ?>
                                <tfoot>
                                    <tr><td></td><td><h3>$<?php _e($total); ?> <small>Total</small></h3></td></tr>
                                    <tr><td colspan="2" class="text-right"><img width="220px" src="https://shoutcart.com/assets/images/cc.png" /><br/><small class="text-muted">PayPal and CC available on orders over $50</small></td></tr>
                                </tfoot>
                            <?php endif; ?>
                        </table>
                    </div>
                </div>
                <?php if($showSuccessInvitation  == 200): ?>
                    <div class="row">
                        <div class="alert alert-success">
                            <span><strong>
                                Successfully Send The Request To Influencers!
                            </strong></span>
                        </div>
                    </div>
                <?php elseif($showSuccessInvitation == 101): ?>
                    <div class="row">
                        <div class="alert alert-danger">
                            <span><strong>
                                Please Check Your Balance. Need To add Some Fund!
                            </strong></span>
                        </div>
                    </div>
                <?php endif ?>
                <div class="row"></div>
                    <div class="panel panel-default">
                        <div class="panel-heading">Your Shoutout Details</div>
                        <div class="panel-body order-form">
                        <form action="" method="post" name="cart" enctype="multipart/form-data">
                <div class="form-group label-floating">
                    <label class="label">Description</label>
                    <textarea name="description" rows="8" class="form-control gp-description"> </textarea>
                </div>                           
                        
                            <input type="hidden" name="order_submit" value="1">
                            <!--
                        <div class="form-group">
                            <label for="email">Email</label>
                            <p class="text-muted"><small>Purchase receipt will be sent to this address.</small></p>
                            <input name="email" type="email" required class="form-control" id="email" placeholder="Email" value="                             shuvosaha5613@gmail.com
                            ">
                        </div>
                        <div class="form-group">
                            <label for="caption">Shoutout Caption</label>
                            <p class="text-muted"><small>Caption for your image or video. <strong>You MUST include your <span>@</span>username here.</strong> Influencers will post EXACTLY what you write in here. You can include emojis and basic formatting here as well.</small></p>
                            <p class="text-muted"><small>&bull; For Twitter tweets, make sure your caption is under 130 characters.</small></p>
                            <p class="text-muted"><small>&bull; For Instagram shoutouts, do not include more than 30 #hashtags.</small></p>
                            <p class="text-danger"><small>Note: We will add a 6-character shortcode at the end of your caption to track statistics!</small></p>
                            <textarea name="caption" maxlength="990" required class="form-control" id="caption">
                                
                            </textarea>
                            <script>
                                CKEDITOR.replace('caption');
                            </script>
                        </div>
                        <div class="form-group">
                            <label for="file">Shoutout Image/Video</label>
                            <p class="text-muted"><small>Please upload an image or 60 second video that you are promoting. Limit is 1 file, 50mb.</small></p>
                            <input name="file" type="file" id="file">
                        </div>
                        <div class="form-group">
                            <label for="datetimepicker">Prefered Shoutout Time</label>
                            <p class="text-muted"><small>Please specify approximate date and time you want the shoutout to be posted. PST (GMT-8) timezone. Allow at least <strong>12hrs in advance</strong> for influencers to prepare your shoutout.</small></p>
                        </div>
                        <div class="col-md-4">
                            <input type="datetime-local" name="time" class="form-control">
                        </div> -->

                        <div class="row">
                            <button type="submit" class="btn btn-success" style="text-align:center;">Check Out</button>
                        </div>
                        </form>
                    </div>
                </div>
                                
            </div>
        </div>

    </div>
</div>

<!-- Footer Section Start -->
<!-- <footer>

    <div class="container">
        <div class="copyright">
            <div class="pull-left"><p>&copy; Shoutcart</p></div>
            <div class="pull-right">
                <a href="http://blog.shoutcart.com/" target="_blank">Blog</a> &bull;
                                    <a href="/affiliate">Affiliate</a> &bull;
                                <a href="http://help.shoutcart.com" target="_blank">Help</a> &bull;
                <a href="/page/terms">Terms</a> &bull;
                <a href="/page/privacy">Privacy</a> &bull;
                <a href="/contact">Contact</a>
            </div>
        </div>
    </div> -->





<!-- <div class="container">
    <div class="content">
        <div class="col-lg-12 col-md-12">
            <div class="panel panel-default" style="margin-top:2em">
                <div class="panel-heading"> Details </div>
                <div class="panel-body">
                    <table class="table">
                        <tr>
                            <th class="col-lg-5" > Username</th>
                            <th class="col-lg-3" > Followers</th>
                            <th class="col-lg-2" > Duration</th>
                            <th class="col-lg-2" > Price</th>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">ShoutOut Details</div>
                <div class="form-group">
                    <p class="text-muted">
                        <small>* Caption for your image or video. You MUST include your @username here. Influencers will post EXACTLY what you write in here. You can include emojis and basic formatting here as well.</small>
                    </p>
                    <p class="text-muted">
                        <small>* For Instagram shoutouts, do not include more than 30 #hashtags.</small>
                    </p>
                </div>
                <div class="form-group label-floating">
                    <label class="label">Description</label>
                    <textarea name="description" rows="8" class="form-control gp-description"> </textarea>
                </div>
                <div class="form-control">
                    <label class="label">Add Image Or Video</label>
                    <textarea rows="8"></textarea>
                </div>
            </div>
        </div>
    </div>
</div> -->
                   

<?php


get_footer();
?>