<?php
/**
 * Template Name: Create Package Page
 *
 * @package gphoebe
 */

/**
 * Check user logged in
 */
not_admin();

check_user_logged_in();

$successCreatePackage = false;

if ($_SERVER['REQUEST_METHOD'] == "POST") {
	if (array_key_exists('create_package', $_REQUEST)) {
		$successCreatePackage = create_package();
    }
}

get_header();

?>

<div class="wrapper">
    <?php
        gphoebe_sidebar();
    ?>
    <div class="main-panel">
        <nav class="navbar navbar-transparent navbar-absolute">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="#"> Create Package </a>
                    </div>
                </div>
        </nav>
        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12 col-md-12">
                        <div class="card card-nav-tabs">
                            <div class="card-header" data-background-color="red">
                                <div class="nav-tabs-navigation">
                                    <div class="nav-tabs-wrapper">
                                        <!-- <span class="nav-tabs-title">Tasks:</span> -->
                                        <ul class="nav nav-tabs" data-tabs="tabs">
                                            <li class="<?php create_package_route_active('all_packages'); ?>">
                                                <a href="<?php create_package_route_gen('all_packages'); ?>" data-toggle="tab">
                                                    All Packages
                                                    <div class="ripple-container"></div>
                                                </a>
                                            </li>
                                            <li class="<?php create_package_route_active('add_package'); ?>">
                                                <a href="<?php create_package_route_gen('add_package'); ?>" data-toggle="tab">
                                                    Add Package
                                                    <div class="ripple-container"></div>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>


                            </div>
                            <?php if(create_package_content('all_packages')): ?>
                                <div class="card-content">
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="profile">
                                            <form><!-- 
                                                <button type="submit" class="btn btn-primary pull-right">Update Profile</button>
                                                <div class="clearfix"></div> -->
                                            </form>
                                        </div>
                                        <div class="card-content">
                                            <div class="tab-content">
                                                <div class="tab-pane active" id="profile">
                                                    <table class="table">
                                                        <thead>
                                                            <tr>
                                                                <td>Description</td>
                                                                <td>Duration</td>
                                                                <td>Price</td>
                                                                <td>Display To Buyer first?</td>
                                                                <td>Action</td>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <?php foreach (get_all_packages() as $packages): ?>
                                                            <tr>
                                                                <td><?php _e($packages->description); ?></td>
                                                                <td><?php _e($packages->duration . ' hr' ) ; ?></td>
                                                                <td><?php  _e('$ ' .$packages->price); ?></td>
                                                                <?php if($packages->display): ?>
                                                                    <td>Yes</td>
                                                                <?php else: ?>
                                                                    <td>No</td>
                                                                <?php endif; ?>
                                                                <td>
                                                                    <button class="btn btn-danger" style="padding: 8px 10px;">Remove</button>
                                                                </td>
                                                            </tr>
                                                            <?php endforeach; ?>

                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php elseif(user_profile_content('add_package')): ?>
                                <div class="card-content">
                                    <div class="tab-content">
                                        <?php if($successCreatePackage): ?>
                                            <div class="alert alert-success">
                                                <span><strong>
                                                        Successfully Created!
                                                    </strong></span>
                                            </div>
                                        <?php endif; ?>
                                        <div class="tab-pane active" id="profile">
                                           <form method="POST">
                                               <input type="hidden" name="create_package">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group label-floating">
                                                            <label class="control-label">Time Duration (in hours)</label>
                                                            <input type="text" name="duration" class="form-control package-time">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group label-floating">
                                                            <label class="control-label">Cost</label>
                                                            <input type="text" name="price" class="form-control package-cost">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-8">
                                                        <div class="form-group label-floating">
                                                            <label class="control-label">Description</label>
                                                            <input type="text" name="description" class="form-control package-description">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group label-floating">
                                                            <label class="control-label">Prefer to show at first?</label>
                                                            <select name="display" class="form-control package-display">
                                                                <option value="1">Yes</option>
                                                                <option value="0">No</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <button type="submit" class="btn btn-primary pull-right">Create Package</button>
                                                    <div class="clearfix"></div>
                                                </div>
                                        </form>
                                        <!-- <div class="tab-pane" id="messages">
                                            <table class="table">
                                                <tbody>
                                                <tr>
                                                    <td>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="optionsCheckboxes" checked>
                                                            </label>
                                                        </div>
                                                    </td>
                                                    <td>Flooded: One year later, assessing what was lost and what was found when a ravaging rain swept through metro Detroit
                                                    </td>
                                                    <td class="td-actions text-right">
                                                        <button type="button" rel="tooltip" title="Edit Task" class="btn btn-primary btn-simple btn-xs">
                                                            <i class="material-icons">edit</i>
                                                        </button>
                                                        <button type="button" rel="tooltip" title="Remove" class="btn btn-danger btn-simple btn-xs">
                                                            <i class="material-icons">close</i>
                                                        </button>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="optionsCheckboxes">
                                                            </label>
                                                        </div>
                                                    </td>
                                                    <td>Sign contract for "What are conference organizers afraid of?"</td>
                                                    <td class="td-actions text-right">
                                                        <button type="button" rel="tooltip" title="Edit Task" class="btn btn-primary btn-simple btn-xs">
                                                            <i class="material-icons">edit</i>
                                                        </button>
                                                        <button type="button" rel="tooltip" title="Remove" class="btn btn-danger btn-simple btn-xs">
                                                            <i class="material-icons">close</i>
                                                        </button>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="tab-pane" id="settings">
                                            <table class="table">
                                                <tbody>
                                                <tr>
                                                    <td>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="optionsCheckboxes">
                                                            </label>
                                                        </div>
                                                    </td>
                                                    <td>Lines From Great Russian Literature? Or E-mails From My Boss?</td>
                                                    <td class="td-actions text-right">
                                                        <button type="button" rel="tooltip" title="Edit Task" class="btn btn-primary btn-simple btn-xs">
                                                            <i class="material-icons">edit</i>
                                                        </button>
                                                        <button type="button" rel="tooltip" title="Remove" class="btn btn-danger btn-simple btn-xs">
                                                            <i class="material-icons">close</i>
                                                        </button>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="optionsCheckboxes" checked>
                                                            </label>
                                                        </div>
                                                    </td>
                                                    <td>Flooded: One year later, assessing what was lost and what was found when a ravaging rain swept through metro Detroit
                                                    </td>
                                                    <td class="td-actions text-right">
                                                        <button type="button" rel="tooltip" title="Edit Task" class="btn btn-primary btn-simple btn-xs">
                                                            <i class="material-icons">edit</i>
                                                        </button>
                                                        <button type="button" rel="tooltip" title="Remove" class="btn btn-danger btn-simple btn-xs">
                                                            <i class="material-icons">close</i>
                                                        </button>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="optionsCheckboxes">
                                                            </label>
                                                        </div>
                                                    </td>
                                                    <td>Sign contract for "What are conference organizers afraid of?"</td>
                                                    <td class="td-actions text-right">
                                                        <button type="button" rel="tooltip" title="Edit Task" class="btn btn-primary btn-simple btn-xs">
                                                            <i class="material-icons">edit</i>
                                                        </button>
                                                        <button type="button" rel="tooltip" title="Remove" class="btn btn-danger btn-simple btn-xs">
                                                            <i class="material-icons">close</i>
                                                        </button>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div> -->
                                    </div>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>



<?php

get_footer();