<?php
/**
 * Template Name: Favorite Page
 * Created by PhpStorm.
 * User: rafsan
 * Date: 4/1/18
 * Time: 4:52 PM
 */

/**
 * Check user logged in
 */
not_admin();

check_user_logged_in();

get_header();

?>

<div class="container">
	<div class="content">

		<ol class="breadcrumb" style="margin-top:1em;">
			<li><a href="/dashboard">Dashboard</a></li>
			<li class="active">Favorites</li>
		</ol>

		<div class="page-header" style="height: 100px;">
			<h1>Favorites</h1>
		</div>
		<div class="row">
			<table class="table">
				<thead>
					<tr>
						<th><strong>Account</strong></th>
						<th><strong>Action</strong></th>
					</tr>
				</thead>

				<tbody>
					<?php foreach(get_favorites() as $value): ?>
						<tr>
							<td><h4><?php _e('@'.$value->user_name); ?></h4></td>
							<td>
								<a href="<?php _e(home_url('/profile?u=' . $value->user_name)); ?>"><button class="btn btn-info">View</button></a>
							</td>
						</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
		</div>
	</div>
</div>

<?php

get_footer();

?>
