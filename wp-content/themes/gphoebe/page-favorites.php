<?php
/**
 * Template Name: favorites Page
 *
 * @package gphoebe
 */
not_admin();

check_user_logged_in();

get_header();

?>

<div class="wrapper">
	<?php
		gphoebe_sidebar();
	?>
	<div class="main-panel">
		<nav class="navbar navbar-transparent navbar-absolute">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="#"> Favorites </a>
                    </div>
                </div>
        </nav>
        <div class="content">
        	<div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12 col-md-12">
                        <div class="card card-nav-tabs">
                            <div class="card-header" data-background-color="red">
                                <div class="nav-tabs-navigation">
                                    <div class="nav-tabs-wrapper">
                                        <!-- <span class="nav-tabs-title">Tasks:</span> -->
                                        <ul class="nav nav-tabs" data-tabs="tabs">
                                            <li class="<?php influencer_section_route_active('all_accounts'); ?>">
                                                <a href="<?php influencer_section_route_gen('all_accounts'); ?>" data-toggle="tab">
                                                    <i class="material-icons">credit_card</i>All Social Account
                                                    <div class="ripple-container"></div>
                                                </a>
                                            </li>
                                            <li class="<?php influencer_section_route_active('add_account'); ?>">
                                                <a href="<?php influencer_section_route_gen('add_account'); ?>" data-toggle="tab">
                                                    <i class="material-icons">credit_card</i>Add Social Account
                                                    <div class="ripple-container"></div>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
					        <?php if(influencer_section_tab_content('all_accounts')): ?>
                                <div class="card-content">
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="profile">
                                            <table class="table">
                                                <tr>
                                                    <th class="col-lg-5" > Username</th>
                                                    <th class="col-lg-5" > Followers</th>
                                                    <th class="col-lg-2" > Action</th>
                                                </tr>
                                                <tbody id="social-accounts">

                                                </tbody>
                                            </table>
                                        </div>
                                        
                                    </div>
                                </div>
					        <?php elseif(user_profile_content('add_account')): ?>
                                <div class="card-content">
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="profile">
<!--                                            <form>-->
                                                <div class="row">
                                                    <div class="col-md-8">
                                                        <div class="form-group label-floating">
                                                            <label class="control-label">Instagram Username</label>
                                                            <input type="text" class="form-control instagram-username">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <button type="submit" id="verify-instagram" class="btn btn-primary pull-right">Verify</button>
                                                    </div>
                                                </div>

<!--                                                <button type="submit" class="btn btn-primary pull-right">Update Password</button>-->
                                                <div class="clearfix"></div>
<!--                                            </form>-->
                                        </div>
                                        <div class="tab-pane" id="messages">
                                            <table class="table">
                                                <tbody>
                                                <tr>
                                                    <td>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="optionsCheckboxes" checked>
                                                            </label>
                                                        </div>
                                                    </td>
                                                    <td>Flooded: One year later, assessing what was lost and what was found when a ravaging rain swept through metro Detroit
                                                    </td>
                                                    <td class="td-actions text-right">
                                                        <button type="button" rel="tooltip" title="Edit Task" class="btn btn-primary btn-simple btn-xs">
                                                            <i class="material-icons">edit</i>
                                                        </button>
                                                        <button type="button" rel="tooltip" title="Remove" class="btn btn-danger btn-simple btn-xs">
                                                            <i class="material-icons">close</i>
                                                        </button>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="optionsCheckboxes">
                                                            </label>
                                                        </div>
                                                    </td>
                                                    <td>Sign contract for "What are conference organizers afraid of?"</td>
                                                    <td class="td-actions text-right">
                                                        <button type="button" rel="tooltip" title="Edit Task" class="btn btn-primary btn-simple btn-xs">
                                                            <i class="material-icons">edit</i>
                                                        </button>
                                                        <button type="button" rel="tooltip" title="Remove" class="btn btn-danger btn-simple btn-xs">
                                                            <i class="material-icons">close</i>
                                                        </button>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="tab-pane" id="settings">
                                            <table class="table">
                                                <tbody>
                                                <tr>
                                                    <td>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="optionsCheckboxes">
                                                            </label>
                                                        </div>
                                                    </td>
                                                    <td>Lines From Great Russian Literature? Or E-mails From My Boss?</td>
                                                    <td class="td-actions text-right">
                                                        <button type="button" rel="tooltip" title="Edit Task" class="btn btn-primary btn-simple btn-xs">
                                                            <i class="material-icons">edit</i>
                                                        </button>
                                                        <button type="button" rel="tooltip" title="Remove" class="btn btn-danger btn-simple btn-xs">
                                                            <i class="material-icons">close</i>
                                                        </button>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="optionsCheckboxes" checked>
                                                            </label>
                                                        </div>
                                                    </td>
                                                    <td>Flooded: One year later, assessing what was lost and what was found when a ravaging rain swept through metro Detroit
                                                    </td>
                                                    <td class="td-actions text-right">
                                                        <button type="button" rel="tooltip" title="Edit Task" class="btn btn-primary btn-simple btn-xs">
                                                            <i class="material-icons">edit</i>
                                                        </button>
                                                        <button type="button" rel="tooltip" title="Remove" class="btn btn-danger btn-simple btn-xs">
                                                            <i class="material-icons">close</i>
                                                        </button>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="optionsCheckboxes">
                                                            </label>
                                                        </div>
                                                    </td>
                                                    <td>Sign contract for "What are conference organizers afraid of?"</td>
                                                    <td class="td-actions text-right">
                                                        <button type="button" rel="tooltip" title="Edit Task" class="btn btn-primary btn-simple btn-xs">
                                                            <i class="material-icons">edit</i>
                                                        </button>
                                                        <button type="button" rel="tooltip" title="Remove" class="btn btn-danger btn-simple btn-xs">
                                                            <i class="material-icons">close</i>
                                                        </button>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
					        <?php endif; ?>
                        </div>
                    </div>
                </div>
        	</div>
        </div>
	</div>

</div>



<?php

get_footer();