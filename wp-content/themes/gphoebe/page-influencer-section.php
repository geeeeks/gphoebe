<?php
/**
 * Template Name: Influencer Section Page
 *
 * @package gphoebe
 */

/**
 * Check user logged in
 */
not_admin();

check_user_logged_in();

$profileStatus = false;

$removeAccount = false;

if($_SERVER['REQUEST_METHOD'] == 'POST') {
    if(array_key_exists('add_influencer', $_REQUEST)) {
       $profileStatus = verify_instagram();
    }

    if(array_key_exists('remove_account', $_REQUEST)) {
        $removeAccount = remove_account();
    }
}

get_header();

?>

<div class="wrapper">
	<?php
		gphoebe_sidebar();
	?>
	<div class="main-panel">
		<nav class="navbar navbar-transparent navbar-absolute">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="#"> Influencer Section </a>
                    </div>
                </div>
        </nav>
        <div class="content">
        	<div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12 col-md-12">
                        <div class="card card-nav-tabs">
                            <div class="card-header" data-background-color="red">
                                <div class="nav-tabs-navigation">
                                    <div class="nav-tabs-wrapper">
                                        <!-- <span class="nav-tabs-title">Tasks:</span> -->
                                        <ul class="nav nav-tabs" data-tabs="tabs">
                                            <li class="<?php influencer_section_route_active('all_accounts'); ?>">
                                                <a href="<?php influencer_section_route_gen('all_accounts'); ?>" data-toggle="tab">
                                                    All Social Account
                                                    <div class="ripple-container"></div>
                                                </a>
                                            </li>
                                            <li class="<?php influencer_section_route_active('add_account'); ?>">
                                                <a href="<?php influencer_section_route_gen('add_account'); ?>" data-toggle="tab">
                                                    Add Social Account
                                                    <div class="ripple-container"></div>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
					        <?php if(influencer_section_tab_content('all_accounts')): ?>
                                <div class="card-content">
                                    <div class="tab-content">
                                        <?php if($removeAccount) : ?>
                                            <div class="alert">
                                                <span>
                                                    <strong>
                                                        Successfully Deleted, Add new Social Account.
                                                    </strong>
                                                </span>
                                            </div>
                                        <?php endif; ?>
                                        <div class="tab-pane active" id="profile">
                                            <table class="table">
                                                <tr>
                                                    <th class="col-lg-5" > Username</th>
                                                    <th class="col-lg-5" > Followers</th>
                                                    <th class="col-lg-2" > Action</th>
                                                </tr>
                                                <tbody id="social-accounts">
                                                    <?php foreach(my_social_account() as $value): ?>

                                                      <tr>
                                                          <td><?php _e($value->user_name); ?></td>
                                                          <td><?php _e($value->followers); ?></td>
                                                          <td>
                                                              <form method="POST">
                                                                <input type="hidden" name="remove_account">
                                                                <input type="hidden" name="atype" value="<?php _e( $value->account_type); ?>">
                                                                  <input type="hidden" name="social_user_id" value="<?php _e($value->social_user_id); ?>">

                                                                  <button class="btn btn-danger" style="padding: 8px 10px;">Remove</button>
                                                              </form>
                                                          </td>
                                                      </tr>
                                                    <?php endforeach; ?>
                                                </tbody>
                                            </table>
                                        </div>
                                        
                                    </div>
                                </div>
					        <?php elseif(user_profile_content('add_account')): ?>
                                <div class="card-content">
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="profile">
                                            <?php if($profileStatus == 200): ?>
                                                <div class="alert alert-success">
                                                    <span>
                                                        <strong>
                                                            Successfully Verified!
                                                        </strong>
                                                    </span>
                                                </div>
                                            <?php elseif($profileStatus == 400): ?>
                                                <div class="alert alert-danger">
                                                    <span>
                                                        <strong>
                                                            Not Verified! Try Again.
                                                        </strong>
                                                    </span>
                                                </div>
                                            <?php elseif($profileStatus == 301): ?>
                                                <div class="alert alert-warning">
                                                    <span>
                                                        <strong>
                                                            Username Missing!
                                                        </strong>
                                                    </span>
                                                </div>
                                            <?php elseif($profileStatus == 500): ?>
                                                <div class="alert alert-danger">
                                                    <span>
                                                        <strong>
                                                            You Can Add one Instagram Account. Social Account Already Exists!
                                                        </strong>
                                                    </span>
                                                </div>
                                            <?php endif; ?>
                                            <form action="" method="POST">
                                                <input type="hidden" name="add_influencer">
                                                <div class="row">
                                                    <div class="col-md-8">
                                                        <div class="form-group label-floating">
                                                            <label class="control-label">Instagram Username</label>
                                                            <input type="text" class="form-control instagram-username"
                                                            name="username">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <button type="submit" id="verify-instagram" class="btn btn-primary pull-right">Verify</button>
                                                    </div>
                                                </div>
                                                </form>
                                            </div>
                                        </div>

                                    </div>
                                </div>
					        <?php endif; ?>
                        </div>
                    </div>
                </div>
        	</div>
        </div>
	</div>

</div>



<?php

get_footer();