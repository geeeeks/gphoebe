<?php
/**
 * Template Name: Login Page
 *
 * @package gphoebe
 */

get_header();

?>

<?php
	$args = array(
                        'echo'           => true,
                        'redirect'       => home_url( '/my-account/' ), 
//                        'form_id'        => 'loginform',
                        'form_class'	 => 'form-horizontal',
                        'label_username' => __( 'Username' ),
                        'label_password' => __( 'Password' ),
                        'label_remember' => __( 'Remember Me' ),
                        'label_log_in'   => __( 'Log In' ),
                        'id_username'    => 'user_login',
                        'id_password'    => 'user_pass',
                        'id_remember'    => 'rememberme',
                        'id_submit'      => 'wp-submit',
                        'remember'       => true,
                        'value_username' => NULL,
                        'value_remember' => true
                    );
                    
                    // Calling the login form.


	if(is_user_logged_in()) {
		echo 'Hello Rafsan';
		echo "<script>document.location = '/my-account/';</script>";
	} else {
		if( $_SERVER['REQUEST_METHOD'] == 'POST' ) {

			$user_login     = esc_attr($_POST["user_email"]);
			$user_password  = esc_attr($_POST["user_password"]);
			$user_email     = esc_attr($_POST["user_email"]);


			$creds = array();
			$creds['user_login'] = $user_login;
			$creds['user_password'] = $user_password;
			$creds['remember'] = true;

			$user = wp_signon( $creds, false );

			if ( is_wp_error($user) )
				echo $user->get_error_message();

			//print_r($user);
			// $user = get_user_by('id', $user->ID);

			// print_r($user);
		}
	}
?>

<div class="container">
            <div class="content">
                <div class="panel-header">
                    <h2 class="text-center">Login or <a href="<?php _e(home_url('/registration'))?>">Register</a></h2>
                </div>
                <div class="panel-body">

                    <div class="col-xs-12">
                        <?php if (register_nonce_check()) : ?>
                        <div class="alert alert-success">
                            <span><strong>Successfully Registerd!</strong></span>
                        </div>
                        <?php endif; ?>
                    </div>

                    <div class="col-xs-12 col-sm-6 col-sm-offset-3">
                        <?php wp_login_form( $args ); ?>
                        Forgot your password? <a href="./reset-password">Reset Password</a><br>
                        Don't have an account? <a href="<?php _e(home_url('/registration'))?>">Register</a>
                    </div>
                </div>
            </div>
        </div>
<?php

get_footer();