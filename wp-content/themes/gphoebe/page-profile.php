<?php
/**
 * Template Name: Profile Page
 *
 * @package gphoebe
 */

get_header();

/**
 * get all data of a user using WpFluent
 */
influencer();

if ($_SERVER["REQUEST_METHOD"] == "POST") {

	if(array_key_exists('influencer', $_REQUEST)) {
		cart_add();
	}

	if(array_key_exists('fav', $_REQUEST)) {
		storeFavorite();
	}
    
    //print_r(cart_show());
}

?>
<div class="container">

	<div class="content">

	<!-- 	<ol class="breadcrumb" style="margin-top:1em;">
			<li><a href="../browse.html">Browse</a></li>
			<li><a href="../browse-fashion-style-influencers.html">
                    <?php
                        _e(influencer()->category->category_name);
                    ?>
                </a></li>
			<li class="active">
                <?php
                    _e(influencer()->user_name);
                ?>
            </li>
		</ol>
 -->
		<div class="row" style="margin-top:1em;">
			<div class="col-sm-8">
				<div class="col-xs-12 bg-info shoutout-info">
					<div class="col-sm-5 col-md-5 col-lg-4">
						<img style="height: 150px;width: 150px" class="img-circle img-responsive"
						     src="<?php _e(influencer()->dp); ?>" alt="gphoebe from <?php _e(influencer()->user_name); ?> influencer"
						     >
					</div>

					<div class="col-sm-7 col-md-7 col-lg-8 card-panel blue lighten-3">
						<form method="POST">
							<input type="hidden" name="fav" value="<?php _e(influencer()->user_id); ?>">
	                        <div class="pull-right">
	                            <button class="btn btn-info" data-current-user="<?php _e(get_current_user_id()); ?>"
	                                 data-influencer="<?php _e(influencer()->user_id); ?>"
	                                 data-toggle="tooltip"
	                                 data-placement="bottom"
	                                 id="add_fav"
	                                 title=""
	                                 data-original-title="add to favorites">
	                                 <?php if(fav_or_not(influencer()->user_id)): ?>
	                                    <i class="fa fa-star fa-2x text-muted"></i>
	                                 <?php else: ?>
	                                 	<i class="fa fa-star-o fa-2x text-muted"></i>
	                                 <?php endif; ?>
	                            </button>
	                        </div>
                        </form>
						<div class="pull-left">
							<h2>
                                <?php
                                    _e(influencer()->user_name);
                                ?>
							</h2>
							<p>
								<small>followers:</small><?php _e(convert_follower(influencer()->user_id)); ?></p>
							<p>
								<small>score:</small>
								<?php _e(inf_score(influencer()->user_id)); ?>
								<a class="hidden-xs" data-toggle="tooltip" data-placement="top"
								   title="Shoutcart Score is a proprietary formula that determines authenticity of engagement and followers of the influencer, including their total reach. It is updated on a daily basis, and is unique to each influencer. Higher score = higher reach and engagement!"><i
										class="fa fa-question"></i></a>
								<small class="visible-xs">(engagement %)</small>
							</p>
							
						</div>
					</div>
				</div>

				<div class="clearfix"></div>


				<div class="col-xs-12">
					<div class="row">
						<div class="panel panel-default">
							<div class="panel-heading">Description
							</div>
							<div class="panel-body">
								<div>
                                    <p>
                                        <?php if (influencer()->details) : ?>
                                            <?php _e(influencer()->details->description); ?>
                                        <?php else: ?>
                                            <?php _e('No Description'); ?>
                                        <?php endif; ?>
                                    </p>
								</div>

							</div>
						</div>
					</div>
				</div>

				<div class="clearfix"></div>




				<form method="POST" name="cart">
                    <input type="hidden" name="influencer" value="<?php _e(influencer()->user_id); ?>">
					<div class="panel panel-success">
						<div class="panel-heading">Pricing</div>
						<div class="panel-body">

                            <?php if (! is_user_logged_in()) : ?>
							    <p><i class="fa fa-lock"></i> Pricing information is available to registered
								members only :) Please <a href="<?php _e(gphoebeurl()->login); ?>">Login</a> or <a
									href="<?php _e(gphoebeurl()->register); ?>">Register</a> for free!</p>
						    <?php else: ?>

                                <?php foreach (influencer()->pricing as $key => $value) : ?>
                                    <p>
                                        <label>
                                            <input name="price" type="radio" value="<?php _e($value->price); ?>" checked="<?php $value->display == 1 ? _e('checked') : '' ?>" />
                                            <span><?php _e($value->description) .
                                                        _e(' - $'.$value->price); ?></span>
                                        </label>
                                    </p>
                                <?php endforeach; ?>
                            <div>
                                <button class="btn btn-primary">Add to cart</button>
                            </div>
                            <?php endif; ?>
                        </div>

                        <div class="cleafix"></div>
                        <br>
					</div>
				</form>

			</div>
		</div>
	</div>

</div>

<?php

get_footer();

?>
