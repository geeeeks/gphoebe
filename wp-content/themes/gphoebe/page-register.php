<?php

global $wpdb, $user_ID;
$errors = array();

if ($user_ID) {

	echo "<script>document.location = '/my-account/';</script>";
} else {

	if( $_SERVER['REQUEST_METHOD'] == 'POST' )
	{

		// User Name

		$username = sanitize_text_field($_REQUEST['username']);

		if ( strpos($username, ' ') !== false )
		{
			$errors['username'] = "Sorry, no spaces allowed in usernames";
		}
		if(empty($username))
		{
			$errors['username'] = "Please enter a username";
		} elseif( username_exists( $username ) )
		{
			$errors['username'] = "Username already exists, please try another";
		}

		// Email

		$email = sanitize_email($_REQUEST['email']);
		if( !is_email( $email ) )
		{
			$errors['email'] = "Please enter a valid email";
		} elseif( email_exists( $email ) )
		{
			$errors['email'] = "This email address is already in use";
		}

		if(0 === preg_match("/.{6,}/", $_POST['password']))
		{
			$errors['password'] = "Password must be at least six characters";
		}

		// Check password confirmation_matches
		if(0 !== strcmp($_POST['password'], $_POST['password_confirmation']))
		{
			$errors['password_confirmation'] = "Passwords do not match";
		}

		// Check terms of service is agreed to
//        if($_POST['terms'] != "on")
//        {
//            $errors['terms'] = "You must agree to Terms of Service";
//        }

		if(0 === count($errors))
		{

			$password = sanitize_text_field($_POST['password']);

			$new_user_id = wp_create_user( $username, $password, $email );

			registration_extra_data($new_user_id, $username);

			had_referral_code();

			$my_login_nonce = '/login/?_register_nonce=' . wp_create_nonce('my-login' . $new_user_id) . '&request=' . $new_user_id;

			// You could do all manner of other things here like send an email to the user, etc. I leave that to you.

			$success = 1;

			wp_redirect(home_url($my_login_nonce));

		}
	}
}


?>

<?php
/**
 * Template Name: Registration Page
 *
 * @package gphoebe
 */

get_header();

?>


<div class="container">
    <div class="content" id="form-login">
        <div class="panel-header">
            <h2 class="text-center">
                Register or <a href="<?php _e(home_url('/login'))?>">Login</a>
            </h2>
        </div>
        <div class="panel-body">

            
            <div class="col-xs-12 col-sm-5 col-sm-offset-3">
                <form class="form-horizontal" method="POST" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
            
                    <div class="form-group">
                        <div class="col-xs-12">
                            <strong>Registration Information</strong>
                        </div>
                    </div>
          
                    <div class="form-group">
                        <div class="col-xs-12">
                            <div class="input-group">
                                <span class="input-group-addon"> <i class="fa fa-fw fa-user text-primary"></i>
                                </span>
                                <input type="text" name="username" value=""
                                       class="form-control" placeholder="Username" required=""></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-12">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-fw fa-envelope text-primary"></i>
                                </span>
                                <input type="email" name="email" value="" placeholder="Email"
                                       class="form-control" required="">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-12">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-fw fa-lock text-primary"></i>
                                </span>
                                <input type="password" name="password" placeholder="Password" class="form-control"
                                       required="">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-12">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-fw fa-lock text-primary"></i>
                                </span>
                                <input type="password" name="password_confirmation" placeholder="Confirm Password"
                                       class="form-control" required="">
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-xs-12">
                            <script src="https://www.google.com/recaptcha/api.js" async defer></script>
                        </div>
                        <div class="col-xs-12">
                            <div class="checkbox">
                                <label>
                                    <input name="terms" id="checkbox" type="checkbox" autocomplete="off"> I Agree to
                                    <a
                                            href="../page/terms.html" target="_blank">Terms of Service</a>
                                </label><br/>
                            </div>
                        </div>
                    </div>
                    <div class="form-group ">
                        <div class="col-xs-12">
                            <button type="submit" class="btn btn-primary">Register</button>
                        </div>
                    </div>
                </form>
            </div>

            <div class="col-xs-12 col-sm-3 col-sm-offset-1">
                <?php if(count($errors) > 0): ?>
                    <div class="alert alert-warning">
                         <ul>
                          <?php foreach($errors as $key => $value): ?>
                                <li> <?php _e($value); ?> </li>
                          <?php endforeach; ?>
                          </ul>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>

<?php

get_footer();