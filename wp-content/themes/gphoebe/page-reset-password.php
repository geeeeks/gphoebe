<?php


$errors = array();

if ($_SERVER['REQUEST_METHOD'] == "POST") {


	if(array_key_exists('email', $_REQUEST)) {
		$getUserData = wpFluent()->table('users')->where('user_email', $_REQUEST['email'])
												 ->first();

		if($getUserData) {

			if(0 === preg_match("/.{6,}/", $_REQUEST['password'])) {
				$errors['password'] = "Password must be at least six characters";
			}

			if(! 0 == strcmp($_POST['password'] , $_REQUEST['c_password'])) {
				$errors['confirm_password'] = "Confirmation password do not match";
			}

			if (count($errors) === 0) {
				wp_set_password( $_REQUEST['password'], $getUserData->ID );

				wp_redirect( home_url( '/login' ) );
			}

		}

	}

}
if(is_user_logged_in()) {
		echo 'Hello Rafsan';
		echo "<script>document.location = '/my-account/';</script>";
	} else {
		if( $_SERVER['REQUEST_METHOD'] == 'POST' ) {

			$user_login     = esc_attr($_POST["user_email"]);
			$user_password  = esc_attr($_POST["user_password"]);
			$user_email     = esc_attr($_POST["user_email"]);


			$creds = array();
			$creds['user_login'] = $user_login;
			$creds['user_password'] = $user_password;
			$creds['remember'] = true;

			$user = wp_signon( $creds, false );

			if ( is_wp_error($user) )
				echo $user->get_error_message();

			//print_r($user);
			// $user = get_user_by('id', $user->ID);

			// print_r($user);
		}
	}

// if($_SERVER['REQUEST_METHOD'] == 'POST'){

// 	//$email = $_REQUEST['email'];


// 	if(0 === preg_match("/.{6,}/", $_POST['password']))
// 		{
// 			$errors['password'] = "Password must be at least six characters";
// 		}

// 	if(! 0 == strcmp($_POST['password'] , $_POST['confirm_password']))
// 	{
// 		$errors['confirm_password'] = "Confirmation password do not match";
// 	}

// 	if(email_exists( $email ) && count($errors)===0){

// 		//$email = $_REQUEST['user_email'];
// 		//$user_id = wpFluent()->table('wp_users')->where('user_email',$email)->get('ID');
// 		$user_id = get_user_by( 'email', $email );
// 		//echo $user_id;

// 		$password = sanitize_text_field( $_POST['password'] );

// 		function wp_set_passoword($password,$user_id){

// 			$hash = wp_hash_password( $password );

// 			wpFluent()->table('wp_users')->where('ID', $user_id)->update(array('user_pass'=>$hash));

// 			//wp_cache_delete($user_id->ID 'users');
// 		}

// 		wp_set_password( $password, $user_id->ID );
// 	}
// }
//$P$BuNLPETf5paKavUvta.3Tdp3jQPbI..
?>
<?php
/**
 * Template Name: Reset password Page
 *
 * @package gphoebe
 */

get_header();

?>

<div class="container">
    <div class="content" id="form-login">
        <div class="panel-header">
            <h2 class="text-center">
                Reset Password</a>
            </h2>
        </div>
        <div class="panel-body">
        	<form method="POST"> 
        		<div class="form-group">
                    <div class="col-xs-8">
                        <div class="input-group">
                            <span class="input-group-addon"> <i class="fa fa-fw fa-user text-primary"></i>
                            </span>
                            <input type="text" name="email" value=""
                                   class="form-control" placeholder="Enter Your Email" required=""></div>
                    </div>
                    <br>
                    <div class="form-group">
                        <div class="col-xs-12">
                            <div class="input-group">
                                <span class="input-group-addon"> <i class="fa fa-fw fa-user text-primary"></i>
                                </span>
                                <input type="password" name="password" value=""
                                       class="form-control" placeholder="New Password" required=""></div>
                        </div>
                    </div>
                    <br>
                    <div class="form-group">
                        <div class="col-xs-12">
                            <div class="input-group">
                                <span class="input-group-addon"> <i class="fa fa-fw fa-user text-primary"></i>
                                </span>
                                <input type="password" name="c_password" value=""
                                       class="form-control" placeholder="Confirm Password" required=""></div>
                        </div>
                    </div>
                    <br>
                    <button class="btn btn-primary" style="margin-left: 30px;">Reset Password</button>
                </div>
        	</form>
        </div>
    </div>
</div>

<?php 

get_footer( );
