<?php
/**
 * Template Name: User Profile Page
 *
 * @package gphoebe
 */

not_admin();

if($_SERVER['REQUEST_METHOD'] == "POST") {
    if(array_key_exists('add_update_user_profile', $_REQUEST)) {
        storeUserData();
    }
}

get_header();

?>

<div class="wrapper">
	<?php
		gphoebe_sidebar();
	?>

	<div class="main-panel">
		<nav class="navbar navbar-transparent navbar-absolute">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">User Profile </a>
                </div>
            </div>
        </nav>
        <div class="content">
        	<div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12 col-md-12">
                        <div class="card card-nav-tabs">
                            <div class="card-header" data-background-color="red">
                                <div class="nav-tabs-navigation">
                                    <div class="nav-tabs-wrapper">
                                        <ul class="nav nav-tabs" data-tabs="tabs">
                                            <li class="<?php user_profile_tab('profile_information'); ?>">
                                                <a href="<?php user_profile_tab_route('profile_information'); ?>" data-toggle="tab">
                                                    Profile Information
                                                    <div class="ripple-container"></div>
                                                </a>
                                            </li>
                                            <li class="<?php user_profile_tab('settings'); ?>">
                                                <a href="<?php user_profile_tab_route('settings'); ?>" data-toggle="tab">
                                                    Settings
                                                    <div class="ripple-container"></div>
                                                </a>
                                            </li>

                                            <?php if (! my_details()->is_buyer): ?>
                                                <li class="<?php user_profile_tab('referral_link'); ?>">
                                                    <a href="<?php user_profile_tab_route('referral_link'); ?>" data-toggle="tab">Referral Link
                                                        <div class="ripple-container"></div>
                                                    </a>
                                                </li>
                                            <?php endif; ?>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <?php if(user_profile_content('profile_information')): ?>
                            <div class="card-content">
                                <div class="tab-content">
                                    <div class="tab-pane active" id="profile">
                                        <form method="post">
                                            <input type="hidden" name="add_update_user_profile">
                                            <div class="user-account-form">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group label-floating">
                                                        <label class="control-label">Username</label>
                                                        <input type="text" name="user_name" class="form-control gp-user_name" value=" <?php echo getUserDetails()->user_name; ?>" disabled>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group label-floating">
                                                        <label class="control-label">Phone Number</label>
                                                        <input type="text" name="phone_number" class="form-control gp-phone_number" value=" <?php echo getUserDetails()->phone_number; ?>">
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group label-floating">
                                                        <label class="control-label">Register Date</label>
                                                        <input type="text" name="register_date" class="form-control gp-register_date" disabled value=" <?php echo getUserDetails()->registration_date; ?>">
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group label-floating">
                                                        <label class="control-label">Language</label>
                                                        <select name="languages" class="form-control gp-languages" value=" <?php echo getUserDetails()->language; ?>">
                                                            <?php foreach (languages() as $key => $value) : ?>
                                                                <?php if(getUserDetails()->language == $value): ?>
                                                                    <option value="<?php _e($value); ?>" selected="selected"> <?php _e($value); ?></option>
                                                                <?php else: ?>
                                                                    <option value="<?php _e($value); ?>"> <?php _e($value); ?></option>
                                                                <?php endif; ?>
                                                            <?php endforeach; ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group label-floating">
                                                        <label class="control-label">Country</label>
                                                        <select name="countries" class="form-control gp-countries value=" <?php echo getUserDetails()->country; ?>"">
                                                            <?php foreach (countries() as $key => $value) : ?>
                                                                <?php if(getUserDetails()->country == $value): ?>
                                                                    <option value="<?php _e($value); ?>" selected="selected"> <?php _e($value); ?></option>
                                                                <?php else: ?>
                                                                    <option value="<?php _e($value); ?>"> <?php _e($value); ?></option>
                                                                <?php endif; ?>
                                                            <?php endforeach; ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group label-floating">
                                                        <label class="control-label">Age</label>
                                                        <input type="text" name="age" class="form-control gp-age" value=" <?php echo getUserDetails()->age; ?>">
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group label-floating">
                                                        <label class="control-label">Sex</label>
                                                        <select name="sex" class="form-control gp-sex" value=" <?php echo getUserDetails()->sex; ?>">
                                                            <?php foreach (sex() as $key => $value) : ?>
                                                                <?php if(getUserDetails()->sex == $key): ?>
                                                                    <option value="<?php _e($value); ?>" selected="selected"> <?php _e($value); ?></option>
                                                                <?php else: ?>
                                                                    <option value="<?php _e($value); ?>"> <?php _e($value); ?></option>
                                                                <?php endif; ?>
                                                            <?php endforeach; ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group label-floating">
                                                        <label class="control-label">Activity</label>
                                                        <select name="activity" class="form-control gp-activity" value=" <?php echo getUserDetails()->activity; ?>">
                                                            <?php foreach (activity() as $key => $value) : ?>
                                                                <?php if( $value == 'all') : ?>
                                                                    <option value="<?php _e($value); ?>"> <?php _e($key); ?></option>
                                                                <?php else: ?>
                                                                    <option value="<?php _e($value); ?>"> <?php _e($key .' '. $value); ?></option>
                                                                <?php endif; ?>
                                                            <?php endforeach; ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group label-floating">
                                                        <label class="control-label">Category</label>
                                                        <select name="category" class="form-control gp-category" value=" <?php echo getUserDetails()->category; ?>">
                                                            <?php foreach (category() as $key => $value) : ?>
                                                                <?php if(getUserDetails()->category == $value): ?>
                                                                    <option value="<?php _e($value); ?>" selected="selected"> <?php _e($value); ?></option>
                                                                <?php else: ?>
                                                                    <option value="<?php _e($value); ?>"> <?php _e($value); ?></option>
                                                                <?php endif; ?>
                                                            <?php endforeach; ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group label-floating">
                                                        <label class="control-label">Description</label>
                                                        <textarea name="description" class="form-control gp-description"> <?php echo getUserDetails()->description; ?>  </textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <button type="submit" class="btn btn-primary user-account-button pull-right">Update Profile</button>
                                            <div class="clearfix"></div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <?php elseif(user_profile_content('settings')): ?>
                            <div class="card-content">
                                <div class="tab-content">
                                    <div class="tab-pane active" id="profile">
                                        <form>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group label-floating">
                                                        <label class="control-label">Old Password</label>
                                                        <input type="text" name="old_password" class="form-control">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group label-floating">
                                                        <label class="control-label">New Password</label>
                                                        <input type="text" name="new_password" class="form-control">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group label-floating">
                                                        <label class="control-label">Confirm Password</label>
                                                        <input type="text" name="confirm_password" class="form-control">
                                                    </div>
                                                </div>
                                            </div>

                                            <button type="submit" class="btn btn-primary pull-right">Update Password</button>
                                            <div class="clearfix"></div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <?php elseif(user_profile_content('referral_link')): ?>
                            <div class="card-content">
                                <div class="tab-content">
                                    <div class="tab-pane active" id="profile">

                                            <div class="row">
                                                <div class="col-md-10">
                                                    <div class="form-group label-floating">
                                                        <label class="control-label">Referral Link</label>
                                                        <input type="text" id="refer_link" value="<?php _e(my_referral_link(my_details()->refer_id)); ?>" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>

                                    </div>
                                </div>
                            </div>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
        	</div>
        </div>
	</div>
</div>



<?php


get_footer();