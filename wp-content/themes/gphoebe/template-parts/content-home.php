<?php
/**
 * Template part for displaying Home Page
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package gphoebe
 */

?>

<div class="splash-image">    
    <div class="container">
        <div class="content">
            <div class="title text-center">
                <h1>Gphoebe</h1>
                <h3 style="color: white">Influencer Marketing</h3>               
                <h4 style="color: black">Buy and sell social media shoutouts. Reach over 400M Active Followers Worldwide!</h4>
            </div>
            <div class="text-center"><a href="<?php _e(home_url('/browser')); ?>" class="btn btn-success">Browse Influencers</a></div>
        </div>
    </div>
</div>

<div class="container">
    <div class="content">
           
        <div class="row" style="margin:1em 0 1em 0;">
            <div class="col-md-4">
                <div class="feature text-center">
                    <p><i class="fa fa-globe text-primary" style="font-size:7em;"></i></p>
                    <h4 class="feature-title"><span title="Quality Influencers">Quality Influencers</span></h4>
                    <p>We provide a large marketplace for influencers, offering analytics, engagement metrics and more!</p>
                </div>
            </div>                   
            <div class="col-md-4">
                <div class="feature text-center">
                    <p><i class="fa fa-smile-o text-primary" style="font-size:7em;"></i></p>
                    <h4 class="feature-title"><span title="100% Satisfaction">100% Satisfaction</span></h4>
                    <p>We ensure full security for your money untill the influencer publishes your shoutout.</p>
                </div>
            </div>                 
            <div class="col-md-4">
                <div class="feature text-center">
                    <p><i class="fa fa-bar-chart text-primary" style="font-size:7em;"></i></p>
                    <h4 class="feature-title"><span title="Details and Statistics">Details and Statistics</span></h4>
                    <p>Gphoebe offers tracking and statistics of every shoutout, know exactly what's working and maximize your results!</p>
                </div>
            </div>
        </div>
        
        <!-- <div class="row text-center">
            <br/>
            <p>Find <a href="page/why-shoutcart.html"><i class="fa fa-heart"></i> More Reasons</a> to love Shoutcart.</p>
            <br/>
        </div> -->
            
        <div class="row text-center" style="margin:1em 0 1em 0;">
            <h3 class="heading">Featured Influencers</h3>
        </div>

        <div class="row fetaured-users"></div>

            
        <div class="row text-center">
            <br/>
            <p>Gphoebe connects you with popular influencers and get your product in front of their followers!<br/>400M+ Followers network, simple checkout, guaranteed satisfaction.</p>
            <br/>
            <a class="btn btn-success" href="/browser" role="button">Browse Influencers</a><br/>
            <!-- <a class="btn" href="page/how-it-works.html" role="button">How it Works?</a>
            <br/> -->
        </div>

    </div>
</div>

<!-- <div style="background-color:#f5f5f5; margin-top:5em;">
    <div class="container">
        <div class="row" style="margin:2em 0 2em 0;">
            <div class="col-md-4">
                    <div class="text-center">
                        <p><img width="160" src="assets/images/examples/songplay.png" class="img-circle"></p>
                        <p class="text-muted">Shoutcart allowed us to push our music app through iOS charts and acquire downloads at $0.10 cpi, our most successful launch!</p>
                        <p><i>Songplay By Anobot LLC</i></p>
                    </div>
                </div>                   
                <div class="col-md-4">
                    <div class="text-center">
                        <p><img width="160" src="assets/images/examples/goaltactics.jpg" class="img-circle"></p>                            
                        <p class="text-muted">We drove targeted installs of our game with Shoutcart, it resulted in great chart and search results positions!</p>
                        <p><i>Goal Tactics by Xyrality GmbH</i></p>
                    </div>
                </div>                 
                <div class="col-md-4">
                    <div class="text-center">
                        <p><img width="160" src="assets/images/examples/santino.jpg" class="img-circle"></p>
                        <p class="text-muted">Shoutcart helped us promote a contest for new sneakers. We received great engagement and many followers!</p>
                        <p><i><span>@</span>santinoloconte</i></p>
                    </div>
                </div>
            </div>
            <div class="row text-center">
                <p class="text-muted">More case studies available on our <a href="page/examples.html">Examples</a> page</p>
                <a class="btn btn-success" href="/browser/?c_p=1" role="button">Browse Influencers</a>
                <br/><br/>
            </div>
        </div>
    </div>
</div> -->