const mix      = require('laravel-mix');
const min = '';

mix.js('dev-js/all-accounts.js', `js/all-account.js`);
mix.js('dev-js/create-package.js', `js/create-package.js`);
mix.js('dev-js/customizer.js', `js/customizer.js`);
mix.js('dev-js/gphoebe.js', `js/gphoebe.js`);
mix.js('dev-js/gphoebe-paypal.js', `js/gphoebe-paypal.js`);
mix.js('dev-js/gphoebe-user.js', `js/gphoebe-user.js`);
mix.js('dev-js/navigation.js', `js/navigation.js`);
mix.js('dev-js/skip-link-focus-fix.js', `js/skip-link-focus-fix.js`);
mix.js('dev-js/user-account.js', `js/user-account.js`);